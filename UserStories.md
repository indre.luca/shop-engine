## Title: As a user i want to order an item without Logging in

### Aceptance Criteria

    - Browse the website using the search function and filter the results using the categories
    - Add one or more products to the cart
    - Fill in the bliiling information (personal information, address, courier)
    - Select the payment for the request online or on delivery
    
### Technical Requirements

    - Store items in the database
    - Store the client's order in the database
    - Calculate the Order Total Cost
    - Provide payment options
    
## Title: As a user I want to create an account to save my billing information

### Aceptance Criteria

    - Click on the register button
    - Insert account information
    - Insert billing information
    - Use this bliing information whenever the user places a order
    
### Technical Requirements

    - Store the account information
    - Send a confirmation email
    - Save the user address(es)
    - Display each preset as a button on the billing screen
    
## Title: As a owner I want to add a new product

### Acceptance Criteria

    - Log in to a Moderator Account
    - Go to the Management Page
    - Click on Add Product Button
    - Fill in all the necesary information
    
### Technical Requirement

    - Store the account information
    - Create a management page
    - Store the product information in the database
    