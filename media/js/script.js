$("#editinfo").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), // serializes the form's elements.
        success: function(data)
        {
            alert(data); // show response from the php script.
        }
    });
});
$(".drill_cursor").click(function(){
//do something
});
$(document).ready(function () {
    $(document).on('mouseenter', '.divbutton', function () {
        $(this).find(".view_button").css('opacity','1');
    }).on('mouseleave', '.divbutton', function () {
        $(this).find(".view_button").css('opacity','0');
    });
});