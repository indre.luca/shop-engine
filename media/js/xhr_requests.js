$(document).ready(function () {
    $("#editinfo").submit(function (e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');


        $.ajax({
            type: "POST",
            url: url,
            data: form.serializeArray(), // serializes the form's elements.
            success: function (data) {
            }
        });
    });
});


$(document).ready(function () {
    $(".add_cart_item").submit(function (e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serializeArray(), // serializes the form's elements.
            success: function (data) {
                $("#addToCartAlert").fadeIn();
                closeAddToCartAlert();
            }
        });
    });
});
$(document).ready(function () {
    $(".add_wishlist_item").submit(function (e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serializeArray(), // serializes the form's elements.
            success: function (data) {
                $("#addToWishlistAlert").fadeIn();
                closeAddToWishlistSuccessAlert();
            }
        });
    });
});

function display_products_search(element) {
    event.preventDefault();
    var product_list = document.getElementById("product_list");
    var search = element['search'].value;
    var get_product_list = new XMLHttpRequest();
    get_product_list.open("POST", window.location.origin + "/Store/search");
    get_product_list.onload = function () {
        product_list.innerHTML = get_product_list.responseText;
    };
    get_product_list.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    bodyRequest = 'search=' + search;
    get_product_list.send(bodyRequest);
}

// var userNameCheck = function () {
//     if (document.forms['registerForm']['first_name'].length >= 1) {
//         //Nothing happens
//     } else {
//         alert("User Name cannot be blank");
//         return false;
//     }
// };
// var emailCheck = function checkEmail() {
//
//     var email = document.getElementById('txtEmail');
//     var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//
//     if (!filter.test(email.value)) {
//         alert('Please provide a valid email address');
//         email.focus;
//         return false;
//     }
// };

var passwordCheck = function () {
    if (document.myForm.username.length >= 8) {

    } else {
        alert("User Name cannot be blank");
        return false;
    }
};

function display_products_category(element) {
    event.preventDefault();
    var product_list = document.getElementById("product_list");
    var category = element['category'].value;
    var get_product_list = new XMLHttpRequest();
    get_product_list.open("POST", window.location.origin + "/Store/search");
    get_product_list.onload = function () {
        product_list.innerHTML = get_product_list.responseText;
    };
    get_product_list.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    bodyRequest = 'category=' + category;
    get_product_list.send(bodyRequest);
}

function edit_account_information(element) {
    event.preventDefault();
    var account_information = document.getElementById("account_information");
    var user_id = element['user_id'].value;
    var first_name = element['user_first_name'].value;
    var last_name = element['user_last_name'].value;
    var email = element['user_email'].value;
    var mobile_phone = element['mobile_telephone'].value;
    if (typeof element['landline_telephone'].value !== 'undefined') {
        var landline_phone = element['landline_telephone'].value;
    } else {
        landline_phone = 0;
    }
    var get_product_list = new XMLHttpRequest();
    get_product_list.open("POST", window.location.origin + "/Account/save_information");
    get_product_list.onload = function () {
        account_information.innerHTML = get_product_list.responseText;
    };
    get_product_list.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    bodyRequest = 'user_id=' + user_id + '&user_first_name=' + first_name +
        '&user_last_name=' + last_name +
        '&user_email=' + email +
        '&mobile_telephone=' + mobile_phone +
        '&landline_telephone=' + landline_phone;
    get_product_list.send(bodyRequest);
    $("#informationEditSuccess").fadeIn();
    closeInformationEditSuccess();
}

function edit_address(element) {
    event.preventDefault();
    var product_list = document.getElementById("addresses");
    var address_id = element['address_id'].value;
    if (element['address_name'].value.length > 3) {
        var address_name = element['address_name'].value;
    } else {
    }
    var address_country = element['address_country'].value;
    var address_city = element['address_city'].value;
    var address_address = element['address_address'].value;
    var get_product_list = new XMLHttpRequest();
    get_product_list.open("POST", window.location.origin + "/Account/save_address");
    get_product_list.onload = function () {
        product_list.innerHTML = get_product_list.responseText;
    };
    get_product_list.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    bodyRequest = 'address_id=' + address_id +
        '&address_name=' + address_name +
        '&address_country=' + address_country +
        '&address_city=' + address_city +
        '&address_address=' + address_address;
    get_product_list.send(bodyRequest);
    setTimeout(function () {
    }, 1000);
    $("#editAddressSuccess").fadeIn();
    closeEditAddressSuccess();
}

function save_address(element) {
    event.preventDefault();
    if (typeof element['first_address'].value !== 'undefined') {
        var first_address = element['first_address'].value;
    } else {
        first_address = 0;
    }
    var product_list = document.getElementById("addresses");
    var address_name = element['address_name'].value;
    var address_country = element['address_country'].value;
    var address_city = element['address_city'].value;
    var address_address = element['address_address'].value;
    var get_product_list = new XMLHttpRequest();

    setTimeout(function () {
        get_product_list.open("POST", window.location.origin + "/Account/save_address");
        get_product_list.onload = function () {
            product_list.innerHTML = get_product_list.responseText;
        };
        get_product_list.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        bodyRequest = 'address_name=' + address_name +
            '&address_country=' + address_country +
            '&address_city=' + address_city +
            '&address_address=' + address_address +
        '&first_address=' + first_address;
        get_product_list.send(bodyRequest);
    }, 1000);
    $("#addAddressSuccess").fadeIn();
    closeAddAddressSuccess();
}

function delete_address(element) {
    event.preventDefault();
    var product_list = document.getElementById("addresses");
    var address_id = element['address_id'].value;
    var get_product_list = new XMLHttpRequest();

    setTimeout(function () {
        get_product_list.open("POST", window.location.origin + "/Account/delete_address");
        get_product_list.onload = function () {
            product_list.innerHTML = get_product_list.responseText;
        };
        get_product_list.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        bodyRequest = 'address_id=' + address_id;
        get_product_list.send(bodyRequest);
    }, 1000);
    $("#deleteAddressSuccess").fadeIn();
    closeDeleteAddressSuccess();
}

function set_default(element) {
    event.preventDefault();
    var product_list = document.getElementById("addresses");
    var address_id = element['address_id'].value;
    var get_product_list = new XMLHttpRequest();

    setTimeout(function () {
        get_product_list.open("POST", window.location.origin + "/Account/set_default_address");
        get_product_list.onload = function () {
            product_list.innerHTML = get_product_list.responseText;
        };
        get_product_list.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        bodyRequest = 'address_id=' + address_id;
        get_product_list.send(bodyRequest);
    }, 200);
    $("#defaultAddressSuccess").fadeIn();
    closeSnoAlertBox();
}

function display_products_price(element) {
    event.preventDefault();
    var product_list = document.getElementById("product_list");
    var services_array = [];
    var minPrice = element['min'].value;
    var maxPrice = element['max'].value;
    var get_product_list = new XMLHttpRequest();
    get_product_list.open("POST", window.location.origin + "/Store/search");
    get_product_list.onload = function () {
        product_list.innerHTML = get_product_list.responseText;
    };
    get_product_list.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    bodyRequest = 'min=' + minPrice + '&max=' + maxPrice;
    get_product_list.send(bodyRequest);

}

function send_recovery_email(element) {
    event.preventDefault();
    var email = document.getElementById("email").value;
    $.ajax({
        type: "POST",  //type of method
        url: "/Account/send_recovery_email",  //your page
        data: {email: email},// passing the values
        success: function (res) {
            $("#recoveryEmailSent").fadeIn();
            recoveryEmailSent();
        }
    });
}


function closeAddToCartAlert() {
    window.setTimeout(function () {
        $("#addToCartAlert").fadeOut(300);
    }, 2000);
}

function closeEditAddressFail() {
    window.setTimeout(function () {
        $("#editAddressFail").fadeOut(300);
    }, 2000);
}

function closeEditAddressSuccess() {
    window.setTimeout(function () {
        $("#editAddressSuccess").fadeOut(300);
    }, 2000);
}

function closeInformationEditSuccess() {
    window.setTimeout(function () {
        $("#informationEditSuccess").fadeOut(300);
    }, 2000);
}

function closeDeleteAddressSuccess() {
    window.setTimeout(function () {
        $("#deleteAddressSuccess").fadeOut(300);
    }, 2000);
}

function closeAddAddressSuccess() {
    window.setTimeout(function () {
        $("#addAddressSuccess").fadeOut(300);
    }, 2000);
}

function closeDefaultAddressSuccess() {
    window.setTimeout(function () {
        $("#defaultAddressSuccess").fadeOut(300);
    }, 2000);
}

function closeAddAddressFail() {
    window.setTimeout(function () {
        $("#addAddressFail").fadeOut(300);
    }, 2000);
}

function closeDefaultAddressFail() {
    window.setTimeout(function () {
        $("#defaultAddressFail").fadeOut(300);
    }, 2000);
}

function closeDeleteAddressFail() {
    window.setTimeout(function () {
        $("#deleteAddressFail").fadeOut(300);
    }, 2000);
}

function recoveryEmailSent() {
    window.setTimeout(function () {
        $("#recoveryEmailSent").fadeOut(300);
    }, 2000);
}

function closeInformationEditFail() {
    window.setTimeout(function () {
        $("#informationEditFail").fadeOut(300);
    }, 2000);
}

