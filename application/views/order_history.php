<?php
include 'UI_elements/header.php';
echo HTML::style("../media/css/bootstrap.css"); ?>
<div class="container mt-5">
<!--    --><?php //foreach($orders as $order){ ?>
    <div class="row">
        <div class="col-lg-4 col-md-3 col-sm-12 align-self-center">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="text-center"></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p></p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 align-self-center">
            <div class="row">
                <div class="offset-lg-4 col-lg-4"><h6
                            class="text-right strikethrough">$</h6>
                </div>
                <div class="col-lg-4"><h6 class=""> (-%)</h6></div>
            </div>
            <div class="row">
                <div class="offset-lg-2 col-lg-10 align-self-end"><h6
                            class="text-right">$</h6></div>
            </div>
            <hr>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 align-self-center">
            <form method="post" action="/Cart/add_to_cart">
                <div class="row">
                    <input type="hidden" name="item_id" value=''>
                    <div class="offset-lg-2 col-lg-10 align-self-center">
                        <button type="submit" class="btn btn-success btn-block">Add to Cart</button>
                    </div>
                </div>
            </form>
            <form method="post" action="/Account/remove_from_wishlist">
                <div class="row">
                    <input type="hidden" name="item_id" value=''>
                    <div class="offset-lg-2 col-lg-10 align-self-center">
                        <button type="submit" class="btn btn-danger btn-block">Remove</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!--    --><?php //}?>
</div>