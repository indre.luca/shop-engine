<?php
#Used in editing user information
?>

<!DOCTYPE html>
<html lang="en">
<?php include 'UI_elements/header.php'; ?>
<head>
    <title>Edit Information | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Checkout Page for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, User, Checkout, Billing, Information, User Information, Email, Name">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:25">
</head>
<body>
<main>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Billing Information</div>
            <div class="panel-body">
                <form method="post" action="/Account/save_information">
                    <input type="hidden" name="user_id" id="user_id" value="<?php if (isset($user->id)) echo $user->id; ?>">
                    <div class="form-group">
                        <label for="user_first_name">First Name*</label>
                        <input type="text" name="user_first_name" id="user_first_name" class="form-control" required
                               placeholder="This is the name that will be shown when selecting the address for you order."
                               value="<?php if($user->first_name != NULL) echo $user->first_name ?>">
                    </div>
                    <div class="form-group">
                        <label for="user_last_name">Last Name*</label>
                        <input type="text" name="user_last_name" id="user_last_name" class="form-control" required
                               placeholder="This is the name that will be shown when selecting the address for you order."
                               value="<?php if(isset($user->last_name)) echo $user->last_name ?>">
                    </div>
                    <div class="form-group">
                        <label for="user_email">Email*</label>
                        <input type="text" name="user_email" id="user_email" class="form-control" required
                               placeholder="This is the name that will be shown when selecting the address for you order."
                               value="<?php if (isset($user->email)) echo $user->email; ?>">
                    </div>
                    <div class="form-group">
                        <label for="user_telephone">Mobile Telephone</label>
                        <input type="text" name="user_telephone" id="user_telephone" class="form-control"
                               value="<?php if (isset($user->mobile_telephone)) echo $user->mobile_telephone; ?>">
                    </div>
                    <div class="form-group">
                        <label for="user_telephone">Landline Telephone</label>
                        <input type="text" name="user_telephone" id="user_telephone" class="form-control"
                               value="<?php if (isset($user->landline_telephone)) echo $user->landline_telephone; ?>">
                    </div>
                    <div class="form-group">
                        <p class="text-sm-left" style="font-size: 14px">All fields marked with * are required.</p>
                    </div>
                    <div class="row">
                        <div class="offset-lg-10 col-lg-2 align-self-center mt-5">
                            <button type="submit"
                                    class="btn btn-outline-primary btn-block ">Save Information
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
</body>
</html>