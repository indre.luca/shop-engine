<!doctype html>
<?php include 'UI_elements/header.php';
echo HTML::style("../media/css/bootstrap.css"); ?>
<head>
    <title>There seems to be a problem | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Work In Progress Page of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, Account, Wishlist, User, Products">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:23">
</head>
<title>Site Maintenance</title>
<style>
    body {
        text-align: center;
    }

    h1 {
        font-size: 50px;
    }
    article {
        font: 20px Helvetica, sans-serif;
        color: #333;
        display: block;
        text-align: left;
        width: 800px;
        margin: 0 auto;
        padding: 150px;
        padding-bottom: 25px;
        padding-top: 15px;
    }

    a {
        color: #dc8100;
        text-decoration: none;
    }

    a:hover {
        color: #333;
        text-decoration: none;
    }
</style>
<div class="container pt-5">
    <div class="row">
        <div class="offset-4 col-4 col-centered pt-2">
            <img src="../media/image/WIP.png" alt="..." class="img-fluid">
        </div>
    </div>
</div>
<article>
    <h1>We&rsquo;ll be back soon!</h1>
    <div>
        <p>Sorry for the inconvenience but we&rsquo;re performing some maintenance on this page at the moment. We&rsquo;ll
            be back online shortly!</p>
        <p>&mdash; The Team</p>
    </div>
</article>
<div class="container">
    <div class="row">
        <div class="offset-4 col-4 col-centered">
            <form action="../">
                <input type="submit" class="btn btn-info btn-lg btn-block" value="Back to Main Page">
            </form>
        </div>
    </div>
</div>