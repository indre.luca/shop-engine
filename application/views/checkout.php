<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/20/2019
 * Time: 3:52 PM
 */
include 'UI_elements/header.php';
$user = Auth::instance()->get_user();
echo HTML::style("../media/css/bootstrap.css"); ?>
<head>
    <title>Checkout | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Checkout Page for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, User, Checkout, Billing, Information,">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:25">
</head>
<div class="container-fluid">
    <div class="row">
        <?php if (isset($error)) {
            if ($error == 'address') {
                ?>
                <div class="offset-md-3 col-md-6 alert alert-danger"
                     data-alert="alert">Please add an address to your account.
                </div>
            <?php }
        } ?>
    </div>
    <div class="row">
        <div class="offset-md-3 col-md-6 col-sm-12 mt-3">
            <div class="panel panel-info border rounded pt-2 pb-4" style="background: #e4f1f9"><?php if ($count > 0){ ?>
                <div class="offset-lg-1 offset-md-1 col-lg-10 col-md-10 col-sm-12">
                    <h4 class="card-title font-weight-bold text-center">Checkout</h4>
                    <div class="panel panel-default shadow rounded bg-white">
                        <div class="row">
                            <div class='offset-1 col-10'>
                                <br>
                                <div class="row">
                                    <div class="col-7">
                                        <h6 class="card-subtitle">Product Name</h6>
                                    </div>
                                    <div class="col-2">
                                        <h6 class="card-subtitle text-right">Amount</h6>
                                    </div>
                                    <div class="col-3">
                                        <h6 class="card-subtitle text-right">Price</h6>
                                    </div>
                                </div>
                                <?php
                                $count_products = 0;
                                foreach ($cart_items as $item) {
                                    foreach ($products as $product) {
                                        if ($item->product_id == $product->id) {
                                            $count_products++
                                            ?>
                                            <br>
                                            <div class="row">
                                                <div class="col-7">
                                                    <h6 class="card-subtitle"><?= $count_products . '. ' . $product->product_name ?></h6>
                                                </div>
                                                <div class="col-2">
                                                    <h6 class="card-subtitle text-right"><?= $item->amount ?></h6>
                                                </div>
                                                <div class="col-3">
                                                    <h6 class="card-subtitle text-right"><?= $item->amount * $product->discounted_price ?>
                                                        $</h6>
                                                </div>
                                            </div>
                                        <?php }
                                    }
                                } ?>
                                <br>
                                <div class="row">
                                    <div class="offset-6 col-3">
                                        <h6 class="card-subtitle text-right">Total Cost</h6>
                                    </div>
                                    <div class="col-3">
                                        <h6 class="card-subtitle text-right"><?= $cost ?> $</h6>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                    <br>
                    <h5 class="card-title font-weight-bold">Order Information</h5>
                    <div class="panel panel-default rounded  mt-3" style="background: #e4f1f9">
                        <div class="panel panel-default shadow rounded bg-white">
                            <form action="/Cart/complete_transaction" method="POST">
                                <div class="row">
                                    <div class='offset-1 col-10'>
                                        <br>
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="address_id">Address</label>
                                            </div>
                                            <div class="col-8 form-group">
                                                <select class="form-control" name="address_id"
                                                        id="address_id">
                                                    <?php foreach ($addresses as $address) {
                                                        if ($address->is_default == 1) {
                                                            echo '<option selected value="' . $address->id . '" >' . $address->name . '</option>';
                                                        } else {
                                                            echo '<option value="' . $address->id . '" >' . $address->name . '</option>';
                                                        }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="method_id">Payment Method</label>
                                            </div>
                                            <div class="col-8 form-group">
                                                <select class="form-control" name="method_id"
                                                        id="method_id">
                                                    <?php foreach ($methods as $method) {
                                                        echo '<option value="' . $method->id . '" >' . $method->name . '</option>';
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-4">
                                                <label for="courier_id">Courier</label>
                                            </div>
                                            <div class="col-8 form-group">
                                                <select class="form-control" name="courier_id"
                                                        id="courier_id">
                                                    <?php foreach ($couriers as $courier) {
                                                        echo '<option value="' . $courier->id . '" >' . $courier->name . ' || ' . $courier->price . ' $</option>';
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="offset-4 col-4 align-self-center">
                                                <button formaction="/Cart/View" type="submit" value="Cancel"
                                                        class="btn btn-outline-primary btn-block">Back to Cart
                                                </button>
                                            </div>
                                            <div class="col-4 align-self-center">
                                                <button type="submit" class="btn btn-primary btn-block">Complete Order
                                                </button>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php }else{
                header('Location:../');
                } ?>
            </div>
        </div>
    </div>
</div>

