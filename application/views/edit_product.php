<?php include 'UI_elements/header.php';

echo HTML::style("../media/css/bootstrap.css"); ?>
<head>
    <title>Product Editor | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Product adding for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, Owner,Management, Product, Edit, Enterprise">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:23">
</head>
<div class="container">
    <form action="/Enterprise/save" method="POST">
        <div class="row">
            <input type="hidden" name="product_id" id="product_id" class="form-control" required value="<?= $product->id ?>">
            <div class="col-3">
                <img class="card-img-top" src="<?php echo $product->image_path; ?>"
                     alt="<?php echo $product->product_name; ?>">
            </div>
            <div class="col-9">
                <div class="row">
                    <div class="col-12 form-group">
                        <label for="product_name">Product Name</label>
                        <input type='text' name="product_name" id="product_name" required class="form-control"
                               value="<?php echo $product->product_name; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 form-group">
                        <label for="product_price">Price</label>
                        <input type="number" name="product_price" required id="product_price"
                               class="form-control" value="<?php echo $product->price; ?>">
                    </div>
                    <div class="col-6 form-group">
                        <label for="product_discount">Price after Discount</label>
                        <input type="number" name="product_discount" required id="product_discount"
                               class="form-control" value="<?php echo $product->discounted_price; ?>">
                    </div>
                    <div class="col-6 form-group">
                        <label for="product_stock">Stock</label>
                        <input type="number" name="product_stock" required id="product_stock"
                               class="form-control" value="<?php echo $product->stock; ?>">
                    </div>
                    <div class="col-6 form-group">
                        <label for="product_category">Category</label>
                        <select class="form-control" name="product_category" id="product_category">
                            <?php foreach ($categories as $category)
                                if ($category->id == $product_category['id']) {
                                    echo '<option value="' . $category->id . '" selected>' . $category->name . '</option>';
                                } else {
                                    echo '<option value="' . $category->id . '" >' . $category->name . '</option>';
                                } ?>
                        </select>
                    </div>
                    <div class="col-12 align-self-center">
                        <div class="form-group">
                            <label for="product_description">Description</label>
                            <textarea class="form-control" name="product_description" id="product_description"
                                      rows="3"><?= $product->description ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="offset-6 col-3 align-self-center">
                <button formaction="/Enterprise/Products" type="submit" value="Cancel" class="btn btn-danger btn-block">Cancel</button>
            </div>
            <div class="col-3 align-self-center">
                <button type="submit" value="" class="btn btn-primary btn-block">Save</button>
            </div>
        </div>
    </form>
</div>
