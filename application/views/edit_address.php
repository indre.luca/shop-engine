<!DOCTYPE html>
<html lang="en">
<?php include 'UI_elements/header.php'; ?>
<head>
    <title>Edit Address | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Address Editor Page for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, User, Address, Edit, Billing, Information,">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:25">
</head>
<body>
<main>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Billing Information</div>
            <div class="panel-body">
                <form method="post" action="/Account/save_address">
                    <input type="hidden" name="address_id" id="address_id"
                           value="<?php if (isset($address->country)) echo $address->id; ?>">
                    <div class="form-group">
                        <label for="address_name">Preset Name</label>
                        <input type="text" name="address_name" id="address_name" class="form-control"
                               placeholder="This is the name that will be shown when selecting the address for you order."
                               value="<?php if ($address->name != NULL) echo $address->name ?>">
                    </div>
                    <div class="form-group">
                        <label for="address_country">Country</label>
                        <input type="text" name="address_country" id="address_country" class="form-control"
                               value="<?php if (isset($address->country)) echo $address->country; ?>">
                    </div>
                    <div class="form-group">
                        <label for="address_city">City</label>
                        <input type="text" name="address_city" id="address_city" class="form-control"
                               value="<?php if (isset($address->city)) echo $address->city; ?>">
                    </div>
                    <div class="form-group">
                        <label for="address_address">Address</label>
                        <input type="text" name="address_address" id="address_address" class="form-control"
                               value="<?php if (isset($address->address)) echo $address->address; ?>">
                    </div>
                    <div class="row">
                        <div class="offset-lg-10 col-lg-2 align-self-center mt-5">
                            <button type="submit"
                                    class="btn btn-outline-primary btn-block ">Save Information
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
</body>
</html>