<? if (isset($_GET['page']) && $_GET['page'] > 0) {
    $page = $_GET['page'];
} elseif (isset($page_nr) && $page_nr > 0) {
    $page = $page_nr;
} else {
    $page = 1;
}
$itemsPerPageMin = 16 * ($page - 1);
$itemPerPageMax = $page * 16;
if ($number < $itemPerPageMax) {
    $itemPerPageMax = $number;
}
$count = 1;
?>
<div class="row">
    <?php
    for ($id = $itemsPerPageMin;
         $id < $itemPerPageMax;
         $id++) {
        $id = strval($id); ?>
        <div class="col-md-3 col-sm-12">
            <div class="mb-3 shadow divbutton p-2">
                <form class="form-group mb-0" method="post"
                      action="/Store/details?id=<?= $product[$id]->id; ?>">
                    <a href="#" onclick="$(this).closest('form').submit()">
                        <input type="hidden" name="item_image" value='<?php echo $product[$id]->image_path; ?>'>
                        <input type="hidden" name="item_name"
                               value='<?php echo $product[$id]->product_name; ?>'>
                        <input type="hidden" name="description"
                               value='<?php echo $product[$id]->description; ?>'>
                        <input type="hidden" name="item_price" value='<?php echo $product[$id]->price; ?>'>
                        <input type="hidden" name="item_max" value='<?= $product[$id]->stock ?>'>

                        <div class="p-2 ">
                            <img class="card-img-top" src="<?php if ($product[$id]->image_path != NULL)
                                echo $product[$id]->image_path;
                            else
                                echo '../media/image/lorem-ipsum.jpg' ?>"
                                 alt="<?php echo $product[$id]->product_name; ?>">
                            <div class="row">
                                <div class="col align-self-center product_description_div">
                                    <p class="text-center product_name m-0"
                                       style="font-weight: bold;font-size: 13px"><?php echo $product[$id]->product_name; ?></p>
                                </div>
                            </div>
                        </div>
                    </a>
                </form>
                <div>
                    <button type="button"
                            class="btn btn-info btn-block mt-0 mb-2 view_button toggle_modal quick_view"
                            data-toggle="modal" data-target=".bd-example-modal-<?= $product[$id]->id ?>">Quick View <i class="fas fa-angle-double-right"></i>
                    </button>
                    <div id="bd-example-modal-<?= $product[$id]->id ?>"
                         class="modal fade bd-example-modal-<?= $product[$id]->id ?> " tabindex="-1"
                         role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content p-4">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-5 col-md-5">
                                            <img class="card-img-top"
                                                 src="<?php echo $product[$id]->image_path ?>"
                                                 alt="<?php echo $product[$id]->product_name; ?>">
                                            <div class="row">
                                            </div>

                                        </div>
                                        <div class="offset-1 col-sm-6 col-md-6 col-lg-6">
                                            <div class="row">
                                                <div class="offset-11 col-1     text-right">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="col-12">
                                                    <div class="page-header text-center"><h3
                                                                class="page-title text-center"><?php echo $product[$id]->product_name; ?></h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12 col-lg-12">
                                                    <?php if ($product[$id]->price != $product[$id]->discounted_price) {
                                                        $percent = round(100 - $product[$id]->discounted_price * 100 / $product[$id]->price); ?>
                                                        <div class="row">
                                                        <div class="offset-lg-5 col-lg-4 p-0 text-right"><h6 class="">
                                                                (-<?php echo $percent ?>%)</h6></div>
                                                        <div class="col-lg-3 pl-0"><h6 class="text-right">
                                                                <strike><?php echo $product[$id]->price ?>
                                                                    $</strike></h6>
                                                        </div>

                                                        </div><?php } ?>
                                                    <div class="row">
                                                        <div class="offset-lg-2 col-lg-10 align-self-end">
                                                            <h4
                                                                    class="text-right"><?php echo $product[$id]->discounted_price ?>
                                                                $</h4></div>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-12 align-self-center">
                                                            <form method="post"
                                                                  action="/Account/add_to_wishlist">
                                                                <input type="hidden" name="item_id"
                                                                       value='<?php echo $product[$id]->id ?>'>
                                                                <button type="submit"
                                                                        class="btn btn-outline-primary btn-block">
                                                                    Add to Wishlist <i class="fas fa-heart"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                        <div class="col-md-6 col-sm-12 align-self-center">
                                                            <form class="add_cart_item" method="post"
                                                                  action="/Cart/add_to_cart">
                                                                <input type="hidden" name="item_id"
                                                                       value='<?php echo $product[$id]->id ?>'>
                                                                <button type="submit"
                                                                        class="btn btn-primary btn-block">
                                                                    Add to Cart <i class="fas fa-cart-plus"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col align-self-center">
                            <form class="form-group mb-0" method="post"
                                  action="/Store/details?id=<?= $product[$id]->id; ?>">
                                <a href="#" onclick="$(this).closest('form').submit()">
                                    <?php if ($product[$id]->price <= $product[$id]->discounted_price) {
                                        echo '<p id="price" class="btn btn-outline-primary btn-block disabled mt-0 mb-0">' . $product[$id]->price . ' <i class="fas fa-dollar-sign"></i></p>';
                                    } else {
                                        $percent = round(100 - $product[$id]->discounted_price * 100 / $product[$id]->price);
                                        echo '<p id="price" class="btn btn-warning btn-block disabled mt-0 mb-0 pr-0 pl-0"><i class="fas fa-tags"></i> (-' . $percent . '%)  ' . $product[$id]->discounted_price . ' <i class="fas fa-dollar-sign"></i></p>' ;
                                    } ?>
                                </a></form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php } ?>
</div>