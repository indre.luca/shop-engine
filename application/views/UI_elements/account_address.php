<?php $counting = 0;
if ($count <= 0) {
    $modal = 0?>
    <div class="panel panel-default shadow rounded bg-white">
        <div class="row">
            <div class='offset-1 col-10 mt-4'>
                <div class="row">
                    <div class="col-12">
                        <h5 class="font-weight-bold text-center">No billing information has been
                            provided.</h5>
                        <h5 class="font-weight-bold text-center">Please enter your address.</h5>
                    </div>
                </div>
            </div>
            <div class="mt-3 offset-3 col-6 mb-4">
                <div class="row">
                    <div class="col-lg-12 align-self-center">
                        <button type="button" class="btn btn-outline-primary btn-block"
                                data-toggle="modal"
                                data-target=".bd-example-modal-first"><i class="fas fa-plus"></i> Add billing address
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
} else foreach ($addres as $address) {
    if ($counting == 0) {
        echo '<div class="panel panel-default rounded" style="background: #e4f1f9">';
        $counting++;
    } else {
        echo '<div class="panel panel-default rounded  mt-3" style="background: #e4f1f9">';
        $counting++;
    }
    ?>
    <div class="panel panel-default shadow rounded bg-white">
        <div class="row">
            <div class='offset-1 col-md-8 col-sm-10'>
                <br>
                <div class="row">
                    <div class="col-4">
                        <h6 class="card-subtitle">Preset Name</h6>
                    </div>
                    <div class="col-8">
                        <?php if ($address->name != NULL) {
                            echo '<h6 class="card-subtitle">' . $address->name . '</h6>';
                        } else {
                            echo '<h6 class="card-subtitle"> Address ' . $counting . '</h6>';
                        } ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-4">
                        <h6 class="card-subtitle">Country</h6>
                    </div>
                    <div class="col-8">
                        <?php if ($address->country != NULL)
                            echo '<h6 class="card-subtitle">' . $address->country . '</h6>'; ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-4">
                        <h6 class="card-subtitle">City</h6>
                    </div>
                    <div class="col-8">
                        <h6 class="card-subtitle"><?php if ($address->city != NULL)
                                echo $address->city; ?></h6>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-4">
                        <h6 class="card-subtitle">Address</h6>
                    </div>
                    <div class="col-8">
                        <h6 class="card-subtitle"><?php if ($address->address != NULL)
                                echo $address->address; ?></h6>
                    </div>
                </div>
                <br>
            </div>
            <div class="mt-md-4 col-md-2 offset-md-0 offset-sm-1 col-sm-10">
                <form method="post" class="mb-2" onsubmit="set_default(this)">
                    <div class="row mt-2">
                        <input type="hidden" name="address_id" id="address_id" value="<?= $address->id ?>">
                        <?php if($address->is_default ==0){?>
                        <div class="col-lg-12 align-self-center">
                            <button type="submit" class="btn btn-primary btn-block ">Set default
                            </button>
                        </div><?php } else { ?>
                        <div class="col-lg-12 align-self-center">
                            <button disabled class="btn btn-secondary btn-block ">Default</button>
                        </div><?php }?>
                    </div>
                </form>
                <div class="row">
                    <div class="col-lg-12 align-self-center">
                        <button type="button" class="btn btn-outline-primary btn-block"
                                data-toggle="modal"
                                data-target=".bd-example-modal-<?= $address->id ?>">Edit <i class="fas fa-pen"></i>
                        </button>
                    </div>
                </div>
                <form method="post" onsubmit="delete_address(this)">
                    <div class="row mt-2">
                        <input type="hidden" name="address_id" id="address_id" value="<?= $address->id ?>">
                        <div class="col-lg-12 align-self-center">
                            <button type="submit" class="btn btn-outline-danger btn-block p-2"><i class="far fa-trash-alt"> Delete</i>
                            </button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
<?php }
if ($count >= 1) { ?>
<div class="panel panel-default shadow rounded bg-white mt-3">
    <div class="row">
        <div class="mt-3 offset-3 col-6 mb-3">
            <button type="button" class="btn btn-outline-primary text-center" data-toggle="modal"
                    data-target=".bd-example-modal-add"><i class="fas fa-plus"></i> Add Additional Billing Addresses
            </button>
        </div>
        <?php } ?>
    </div>
</div>