<div class="row mt-4">
    <div class="col-4">
        <h6 class="card-subtitle">Full Name:</h6>
    </div>
    <div class="col-8">
        <h6 class="card-subtitle"><?php echo $user->first_name . ' ' . $user->last_name; ?></h6>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-4">
        <h6 class="card-subtitle">E-mail</h6>
    </div>
    <div class="col-8">
        <h6 class="card-subtitle"><?php echo $user->email; ?></h6>
    </div>
</div>
<?php if (isset($user->mobile_telephone)) { ?>
    <hr>
    <div class="row">
        <div class="col-4">
            <h6 class="card-subtitle">Mobile Telephone</h6>
        </div>
        <div class="col-8">
            <h6 class="card-subtitle"><?php echo $user->mobile_telephone; ?></h6>
        </div>
    </div>
<?php }
if ($user->landline_telephone != NULL) { ?>
    <hr>
    <div class="row">
        <div class="col-4">
            <h6 class="card-subtitle">Landline Telephone</h6>
        </div>
        <div class="col-8">
            <h6 class="card-subtitle"><?php echo $user->landline_telephone; ?></h6>
        </div>
    </div>
    <br>
<?php } else echo '<br>' ?>
