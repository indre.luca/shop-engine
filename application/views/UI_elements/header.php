<script src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="../media/js/xhr_requests.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<nav id="navigation" class="navbar navbar-expand-lg navbar-light bg-info">
    <div class="container">
        <a href="../" class="navbar-brand" href="#">
            <img src="/media/image/logo.png" class="d-inline-block align-top" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse ml-auto" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <?php $session = Session::instance();
                if ($session->get('admin') == 1) {
                    echo '<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Admin
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="../Account/Profile">Products</a>
                    <a class="dropdown-item" href="../Account/Wishlist">Users</a>
                    <a class="dropdown-item" href="../Account/Orders">Orders</a>
                    <a class="dropdown-item" href="../Account/Orders">Companies</a>

                </div>
            </li>';
                }
                if ($session->get('owner') == 1) {
                    echo '<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Owner
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="../Enterprise/Products"><i class="fas fa-box"></i> Products</a>
                    <a class="dropdown-item" href="../Store/coming_soon"><i class="fas fa-chart-line"></i> Sales</a>

                </div>
            </li>';
                }
                if (Auth::instance()->get_user() == FALSE) {
                    echo '<button type="button" class="btn btn-outline-light mr-1 ml-1" data-toggle="modal" data-target=".bd-example-modal-login">Login <i class="fas fa-sign-in-alt"></i></button>
<div class="modal fade bd-example-modal-login" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="container">
        <div class="panel panel-default">
        <div class="panel-heading text-center mt-5"><h3>Login</h3></div>
            <div class="panel-body mt-5">
                <form method="post" action="/Account/login_function">
                    <div class="form-group">
                        <label for="loginEmail">User Email</label>
                        <input type="email" name="loginEmail" id="loginEmail" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">Password</label>
                        <input type="password" minlength="8" name="loginPassword" id="loginPassword" class="form-control">
                    </div>
                    <div class="form-check mb-3">
                            <input type="checkbox" name="loginRemember" id="loginRemember" class="form-check-input"
                                   value="0">
                            <label class="form-check-label" for="loginRemember">Remember me</label>
                        </div>
                    <div class="row">
                    <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-recover" data-dismiss="modal">Recover Password</button>
                    </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                    <div class="form-group text-right">
                    <button type="submit" class="btn btn-info">Login <i class="fas fa-sign-in-alt"></i></button>
                    </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
  </div>
</div>
<button type="button" class="btn btn-outline-light mr-1 ml-1" data-toggle="modal" data-target=".bd-example-modal-register">Register</button>
<div class="modal fade bd-example-modal-register" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="container">
        <div class="row">
            <div class="col-md-12 p-0">
                <div class="panel panel-success border shadow-sm p-4">
                    <div class="panel-heading text-center mt-5"><h3>Welcome</h3></div>
                    <div class="panel-body mt-5">
                        <form name="registerForm" method="post" action="/Account/register_function">
                            <div class="form-group">
                                <label for="firstName">First Name</label>
                                <input type="text" name="first_name" id="firstName" required onblur="userNameCheck();" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="lastName">Last Name</label>
                                <input type="text" name="last_name" id="lastName" required onblur="userNameCheck();" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="registerEmail">User Email</label>
                                <input type="email" name="email" id="registerEmail" required class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="registerPassword">Password</label>
                                <input type="password" minlength="8" name="password" required id="registerPassword"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="repeatPassword">Repeat Password</label>
                                <input type="password" minlength="8" name="repeat_password" required id="repeatPassword"
                                       class="form-control">
                            </div>
                            <div class="form-group text-md-right text-center offset-md-2 col-md-8 mt-md-5">
                                <input type="submit" name="register" id="register" class="btn btn-block btn-info"
                                       value="Register">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-recover" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="container">
                <div class="row-fluid">
                <div class="offset-2 col-8 mt-5"><h3>Password Recovery</h3></div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 mt-3">
                    <form method="post" onsubmit="send_recovery_email(this)">
                        <div class="form-group">
                            <label for="repeatPassword">Email</label>
                            <input type="email" minlength="8" name="email" id="email"
                            class="form-control" required>
                        </div>
                        <div class="form-group text-md-right offset-6 col-6 mt-3">
                            <input type="submit" name="recover" id="recover" class="btn btn-block btn-primary"
                            value="Send Recovery Email">
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>';
                } else {
                    echo '<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle  text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Account
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="../Account/Profile"><i class="fas fa-user"></i> Profile</a>
                    <a class="dropdown-item" href="../Account/Wishlist"><i class="fas fa-heart" style="color: blue;"></i> Wishlist</a>
                    <a class="dropdown-item" href="../Store/coming_soon"><i class="fas fa-book"></i> Orders</a>
                </div>
            </li>';
                    echo '<li class="nav-item"><a class="nav-link text-white" href="../Cart/View"><i class="fas fa-cart-plus"></i> Cart </a></li>';
                    echo '<li class="nav-item"><a class="nav-link text-white" href="../Account/logout"><i class="fas fa-sign-out-alt"></i> Logout</a></li>';
                } ?>
            </ul>
        </div>
    </div>
</nav>
