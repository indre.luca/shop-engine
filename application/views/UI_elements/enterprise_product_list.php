<?php if ($count > 0) {
    foreach ($product_id as $id) {
        $product = ORM::factory('Product', $id->product_id)->as_array();
        //    ORM::factory('Products',$product[])?>
        <div class="row">
            <div class="col-md-2 col-sm-12 align-self-center">
                <img class="card-img-top" style="height: 120px; width: 120px;"
                     src="<?php echo $product['image_path'] ?>" alt="">
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-center"><?php echo $product['product_name'] ?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p class="product_details mb-2"><?php echo $product['description'] ?></p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-12 align-self-center">
                <?php if ($product['price'] != $product['discounted_price']) {
                    $percent = round(100 - $product['discounted_price'] * 100 / $product['price']); ?>
                    <div class="row">
                    <div class="col-sm-6 col-md-6 pr-0 text-left"><h6>Discounted Price:</h6>
                    </div>
                    <div class="col-md-2 col-sm-3 m-0 p-0">
                        <h6 class="text-right ">(-<?php echo $percent ?>%)</h6></div>
                    <div class="col-md-4 col-sm-3 ml-0 pl-0">
                        <h6 class="text-right"><?php echo $product['discounted_price'] ?>$</h6>
                    </div>
                    </div><?php } ?>
                <div class="row">
                    <div class="col-sm-8"><h6>Standard Price</h6></div>
                    <div class="col-sm-4 ml-0 pl-0">
                        <h6 class="text-right"><?php echo $product['price'] ?>$</h6></div>
                </div>
                <div class="row">
                    <div class="col-sm-8"><h6>Stock remaining</h6></div>
                    <div class="col-sm-4 ml-0 pl-0">
                        <h6 class="text-right"><?= $product['stock'] ?></h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-12 align-self-center">
                <form method="post" action="/Enterprise/edit_product">
                    <div class="row">
                        <input type="hidden" name="item_id" value='<?php echo $product['id'] ?>'>
                        <div class="col-12 align-self-center pr-2 pl-2">
                            <button type="submit" class="btn btn-outline-primary btn-block pr-0 pl-0">Edit Item <i class="fas fa-pen"></i></button>
                        </div>
                    </div>
                </form>
                <form method="post" action="/Enterprise/delete">
                    <div class="row">
                        <input type="hidden" name="item_id" value='<?php echo $product['id'] ?>'>
                        <div class="col-12 align-self-center pr-2 pl-2">
                            <button type="submit" class="btn btn-outline-danger btn-block">Remove <i class="far fa-trash-alt"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <hr>
    <?php }
} else { ?>
    <div class="row">
        <div class="col-12">
            <div class="alert alert-primary mt-5 text-center">
                Your business currently has no items.
            </div>
        </div>
    </div>
<?php } ?>