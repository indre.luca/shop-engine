<div class="container">
    <div class="row ">
        <div class="offset-2 col-md-8 col-sm-12 mt-3">
            <form method="post" action="/Account/reset">
                <input type="hidden" name="account_id" value="<?=$user_id?>">
                <div class="form-group">
                    <label for="registerPassword">New Password</label>
                    <input type="password" minlength="8" name="password" id="registerPassword"
                           class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="repeatPassword">Repeat Password</label>
                    <input type="password" minlength="8" name="repeat_password" id="repeatPassword"
                           class="form-control" required>
                </div>
                <div class="form-group text-md-right text-center offset-md-2 col-md-8 mt-md-5">
                    <input type="submit" name="register" id="register" class="btn btn-block btn-info"
                           value="Reset Password">
                </div>
            </form>
        </div>
    </div>
</div>
