<?php
foreach ($addres as $address) {?>
<div class="modal fade bd-example-modal-<?= $address->id ?>" tabindex="-1"
     id="myModal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="container">
                <div class="panel panel-default m-3">
                    <div class="panel-body">
                        <form method="post" onsubmit="edit_address(this)">
                            <input type="hidden" name="address_id" id="address_id"
                                   value="<?php if (isset($address->country)) echo $address->id; ?>">
                            <div class="form-group">
                                <label for="address_name">Preset Name</label>
                                <input type="text" name="address_name"
                                       id="address_name"
                                       class="form-control"
                                       placeholder="This is the name that will be shown when selecting the address for you order."
                                       value="<?php if ($address->name != NULL) echo $address->name; else
                                           echo 'Address ' . $counting ?>">
                            </div>
                            <div class="form-group">
                                <label for="address_country">Country</label>
                                <input type="text" name="address_country"
                                       id="address_country" class="form-control"
                                       value="<?php if (isset($address->country)) echo $address->country; ?>">
                            </div>
                            <div class="form-group">
                                <label for="address_city">City</label>
                                <input type="text" name="address_city"
                                       id="address_city"
                                       class="form-control"
                                       value="<?php if (isset($address->city)) echo $address->city; ?>">
                            </div>
                            <div class="form-group">
                                <label for="address_address">Address</label>
                                <input type="text" name="address_address"
                                       id="address_address" class="form-control"
                                       value="<?php if (isset($address->address)) echo $address->address; ?>">
                            </div>
                            <div class="row">
                                <div class="offset-lg-7 col-lg-2 align-self-center mt-3">
                                    <button type="button"
                                            class="btn btn-outline-primary btn-block"
                                            data-dismiss="modal"><i class="fas fa-times"></i> Close
                                    </button>
                                </div>
                                <div class="col-lg-3 align-self-center mt-3">
                                    <button type="submit"
                                            class="btn btn-primary btn-block "><i class="far fa-save"></i> Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>