<div id="alerts">
    <div id="editAddressSuccess" class="alert alert-success error-handling" data-alert="alert">Address has been
        edited.
    </div>
    <div id="recoveryEmailSent" class="alert alert-success error-handling" data-alert="alert">A reset link has been sent
        to the specified email.
    </div>
    <div id="editAddressFail" class="alert alert-danger error-handling" data-alert="alert">An error occurred editing
        your address
    </div>
    <div id="informationEditSuccess" class="alert alert-success error-handling" data-alert="alert">Account information
        has been edited
    </div>
    <div id="informationEditfail" class="alert alert-danger error-handling" data-alert="alert">An error occurred editing
        your account information
    </div>
    <div id="deleteAddressSuccess" class="alert alert-success error-handling" data-alert="alert">Address has been
        deleted
    </div>
    <div id="deleteAddressFail" class="alert alert-danger error-handling" data-alert="alert">An error occurred editing
        your address
    </div>
    <div id="addAddressSuccess" class="alert alert-success error-handling" data-alert="alert">Address successfully
        added
    </div>
    <div id="addAddressFail" class="alert alert-danger error-handling" data-alert="alert">An error occurred editing your
        address
    </div>
    <div id="defaultAddressSuccess" class="alert alert-success error-handling" data-alert="alert">This address is now
        set as default
    </div>
    <div id="defaultAddressFail" class="alert alert-danger error-handling" data-alert="alert">An error occurred setting
        your default address
    </div>
    <div id="addToCartAlert" class="alert alert-success error-handling" data-alert="alert">Item has been added to cart
    </div>
</div>