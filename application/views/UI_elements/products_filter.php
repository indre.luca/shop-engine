<div class="row">
    <div class="col-12">
        <div class="card  shadow">
            <div class="btn-group">
                <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split"
                        data-hover="dropdown" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"><i class="fas fa-filter"></i>
                    Categories
                </button>
                <div class="col-12 dropdown-menu p-0">
                    <?php foreach ($categories as $category) { ?>
                        <form class="col-12 p-0 m-0" onsubmit="display_products_category(this)" method="post">
                            <input type="hidden" name="category" value="<?= $category->id ?>">

                            <div class="panel bg-white border rounded">
                                <div class="col-12 p-0 m-0">
                                    <button type="submit" class="btn btn-outline-info btn-block" role="button">
                                        <?= $category->name ?></button>
                                </div>
                            </div>
                        </form>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card  shadow">
            <div class="btn-group">
                <button type="button" class="btn btn-info dropdown-toggle dropdown-toggle-split"
                        data-hover="dropdown" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false"><i class="fas fa-funnel-dollar"></i>
                    Price
                </button>
                <div class="col-12 dropdown-menu p-0">
                    <div class="panel bg-white border">
                        <form class="col-12 p-0 m-0" onsubmit="display_products_price(this)" method="post">
                            <input type="hidden" name="min" value="1">
                            <input type="hidden" name="max" value="50">
                            <div class="panel bg-white border rounded">
                                <div class="col-12 p-0 m-0">
                                    <button type="submit" class="btn btn-outline-info btn-block" role="button">1$ - 50$</button>
                                </div>
                            </div>
                        </form>
                        <form class="col-12 p-0 m-0" onsubmit="display_products_price(this)" method="post">
                            <input type="hidden" name="min" value="50">
                            <input type="hidden" name="max" value="100">
                            <div class="panel bg-white border rounded">
                                <div class="col-12 p-0 m-0">
                                    <button type="submit" class="btn btn-outline-info btn-block" role="button">50$ - 100$</button>
                                </div>
                            </div>
                        </form>
                        <form class="col-12 p-0 m-0" onsubmit="display_products_price(this)" method="post">
                            <input type="hidden" name="min" value="100">
                            <input type="hidden" name="max" value="200">
                            <div class="panel bg-white border rounded">
                                <div class="col-12 p-0 m-0">
                                    <button type="submit" class="btn btn-outline-info btn-block" role="button">100$ - 200$</button>
                                </div>
                            </div>
                        </form>
                        <form class="col-12 p-0 m-0" onsubmit="display_products_price(this)" method="post">
                            <input type="hidden" name="min" value="200">
                            <input type="hidden" name="max" value="500">
                            <div class="panel bg-white border rounded">
                                <div class="col-12 p-0 m-0">
                                    <button type="submit" class="btn btn-outline-info btn-block" role="button">200$ - 500$</button>
                                </div>
                            </div>
                        </form>
                        <form class="col-12 p-0 m-0" onsubmit="display_products_price(this)" method="post">
                            <input type="hidden" name="min" value="500">
                            <input type="hidden" name="max" value="1000">
                            <div class="panel bg-white border rounded">
                                <div class="col-12 p-0 m-0">
                                    <button type="submit" class="btn btn-outline-info btn-block" role="button">500$ - 1000$</button>
                                </div>
                            </div>
                        </form>
                        <form class="col-12 p-0 m-0" onsubmit="display_products_price(this)" method="post">
                            <input type="hidden" name="min" value="1000">
                            <input type="hidden" name="max" value="999999">
                            <div class="panel bg-white border rounded">
                                <div class="col-12 p-0 m-0">
                                    <button type="submit" class="btn btn-outline-info btn-block" role="button">1000$+</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>