<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/20/2019
 * Time: 3:52 PM
 */
include 'UI_elements/header.php';
$user = Auth::instance()->get_user();
echo HTML::script('../media/js/jquery-3.4.1.min.js');
echo HTML::style("../media/css/bootstrap.css");
echo HTML::style("../media/css/details.css");
include 'UI_elements/error_handling.php' ?>
<head>
    <title>Account Page | Shop Engine</title>
    <link rel="icon" href="../media/image/favicon.png">
    <meta charset="UTF-8">
    <meta name="description"
          content="User Account Page for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, User, Account,Profile, Billing, Information, Address. Management">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:22">

</head>
<div class="container mt-5">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="panel bg-white border rounded pt-2 pb-4 mt-3">
                <div class="col-12">
                    <img class="card-img-top border rounded-circle" src="../media/image/lorem-ipsum.jpg" alt="">
                </div>
                <div class="col-12">
                    <h4 class="card-subtitle text-center mt-2"><?php echo $user->first_name . ' ' . $user->last_name; ?></h4>
                    <hr>
                </div>
                <div class="col-12">
                    <h6 class="card-subtitle text-center mt-2"><a class="btn btn-outline-primary btn-block"
                                                                  role="button" href="/Store/coming_soon">Your
                            Orders</a>
                    </h6>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 mt-3">
            <div class="panel panel-info border rounded pt-2 pb-4" style="background: #e4f1f9">
                <div class="offset-lg-1 offset-md-1 col-lg-10 col-md-10 col-sm-12">
                    <h4 class="card-title font-weight-bold">Account Information</h4>
                    <div class="panel panel-default shadow rounded bg-white">
                        <div class="row">
                            <div class='offset-1 col-8' id="account_information">
                                <?php include 'UI_elements/account_information.php' ?>
                            </div>
                            <div class="mt-md-4 mb-md-0 mb-sm-4 offset-sm-1 offset-md-0 col-sm-10 col-md-2">
                                <button type="button" class="btn btn-outline-primary btn-block" data-toggle="modal"
                                        data-target=".bd-example-modal-edit">Edit <i class="fas fa-pen"></i>


                                </button>
                                <div class="modal fade bd-example-modal-edit" tabindex="-1" role="dialog"
                                     aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="container">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading text-center mt-5"><h3>Account
                                                            Information</h3></div>
                                                    <div class="panel-body">
                                                        <form method="post" onsubmit="edit_account_information(this)">
                                                            <input type="hidden" name="user_id" id="user_id"
                                                                   value="<?php if (isset($user->id)) echo $user->id; ?>">
                                                            <div class="form-group">
                                                                <label for="user_first_name">First Name*</label>
                                                                <input type="text" name="user_first_name"
                                                                       id="user_first_name" class="form-control"
                                                                       required
                                                                       placeholder="This is the name that will be shown when selecting the address for you order."
                                                                       value="<?php if ($user->first_name != NULL) echo $user->first_name ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="user_last_name">Last Name*</label>
                                                                <input type="text" name="user_last_name"
                                                                       id="user_last_name" class="form-control" required
                                                                       placeholder="This is the name that will be shown when selecting the address for you order."
                                                                       value="<?php if (isset($user->last_name)) echo $user->last_name ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="user_email">Email*</label>
                                                                <input type="text" name="user_email" id="user_email"
                                                                       class="form-control" required
                                                                       placeholder="This is the name that will be shown when selecting the address for you order."
                                                                       value="<?php if (isset($user->email)) echo $user->email; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="mobile_telephone">Mobile Telephone</label>
                                                                <input type="text" name="mobile_telephone"
                                                                       id="mobile_telephone" class="form-control"
                                                                       value="<?php if (isset($user->mobile_telephone)) echo $user->mobile_telephone; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="landline_telephone">Landline
                                                                    Telephone</label>
                                                                <input type="text" name="landline_telephone"
                                                                       id="landline_telephone" class="form-control"
                                                                       value="<?php if (isset($user->landline_telephone)) echo $user->landline_telephone; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <p class="text-sm-left"
                                                                   style="font-size: 14px;color: green">Allfields marked
                                                                    with * are required.</p>
                                                            </div>
                                                            <div class="row">
                                                                <div class="offset-lg-7 col-lg-2 align-self-center mt-3">
                                                                    <button type="button"
                                                                            class="btn btn-outline-primary btn-block"
                                                                            data-dismiss="modal"><i class="fas fa-times"></i> Close
                                                                    </button>
                                                                </div>
                                                                <div class="col-lg-3 align-self-center mt-3">
                                                                    <button type="submit"
                                                                            class="btn btn-primary btn-block "><i class="far fa-save"></i> Save
                                                                        Information
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <h4 class="card-title font-weight-bold">Billing Information</h4>
                    <div id="addresses">
                        <?php include 'UI_elements/account_address.php' ?>
                    </div>
                    <div> <!-- modals -->
                        <div id="first_address_modal">
                            <div class="modal fade bd-example-modal-first" tabindex="-1" role="dialog"
                                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="container">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Billing Information</div>
                                                <div class="panel-body">
                                                    <form method="post" onsubmit="save_address(this)">
                                                        <input type="hidden" name="first_address" value="1">
                                                        <div class="form-group">
                                                            <label for="address_name">Preset Name</label>
                                                            <input type="text" name="address_name" id="address_name"
                                                                   class="form-control"
                                                                   placeholder="This is the name that will be shown when selecting the address for you order.">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="address_country">Country</label>
                                                            <input type="text" name="address_country"
                                                                   id="address_country" class="form-control"
                                                                   placeholder="Please enter your country.">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="address_city">City</label>
                                                            <input type="text" name="address_city" id="address_city"
                                                                   class="form-control"
                                                                   placeholder="Please enter your city">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="address_address">Address</label>
                                                            <input type="text" name="address_address"
                                                                   id="address_address" class="form-control"
                                                                   placeholder="Address should be as detailed as possible to facilitate shipping">
                                                        </div>
                                                        <div class="row">
                                                            <div class="offset-lg-7 col-lg-2 align-self-center mt-3">
                                                                <button type="button"
                                                                        class="btn btn-outline-primary btn-block"
                                                                        data-dismiss="modal"><i class="fas fa-times"></i> Close
                                                                </button>
                                                            </div>
                                                            <div class="col-lg-3 align-self-center mt-3">
                                                                <button type="submit"
                                                                        class="btn btn-primary btn-block "><i class="far fa-save"></i> Save
                                                                    Information
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="address_modals">
                            <?php include "UI_elements/account_address_modal.php"; ?>
                        </div>
                        <div id="add_address_modal">
                            <div class="modal fade bd-example-modal-add" tabindex="-1" role="dialog"
                                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="container">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">Add Billing Information</div>
                                                <div class="panel-body">
                                                    <form id="editinfo" method="post"
                                                          onsubmit="save_address(this)">
                                                        <input type="hidden" name="first_address" value="0">
                                                        <div class="form-group">
                                                            <label for="address_name">Preset Name</label>
                                                            <input type="text" name="address_name"
                                                                   id="address_name"
                                                                   class="form-control"
                                                                   placeholder="This is the name that will be shown when selecting the address for you order.">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="address_country">Country</label>
                                                            <input type="text" name="address_country"
                                                                   id="address_country" class="form-control"
                                                                   placeholder="Please enter your country."
                                                                   required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="address_city">City</label>
                                                            <input type="text" name="address_city"
                                                                   id="address_city"
                                                                   class="form-control"
                                                                   placeholder="Please enter your city"
                                                                   required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="address_address">Address</label>
                                                            <input type="text" name="address_address"
                                                                   id="address_address" class="form-control"
                                                                   placeholder="Address should be as detailed as possible to facilitate shipping"
                                                                   required>
                                                        </div>
                                                        <div class="row">
                                                            <div class="offset-lg-7 col-lg-2 align-self-center mt-3">
                                                                <button type="button"
                                                                        class="btn btn-outline-primary btn-block"
                                                                        data-dismiss="modal"><i class="fas fa-times"></i> Close
                                                                </button>
                                                            </div>
                                                            <div class="col-lg-3 align-self-center mt-3">
                                                                <button type="submit"
                                                                        class="btn btn-primary btn-block "><i class="far fa-save"></i> Save
                                                                    Information
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

