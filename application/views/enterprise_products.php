<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/25/2019
 * Time: 5:09 PM
 */
include 'UI_elements/header.php';
echo HTML::style("../media/css/details.css");
echo HTML::style("../media/css/bootstrap.css"); ?>
<head>
    <title>Company Product Management | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Product adding for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, Owner,Management, Product, Edit, Enterprise">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:23">
</head>
<div class="container pl-0 pr-0 mt-5">
    <div class="row">
        <div class="col-md-3 col-sm-12">
            <div class="panel bg-white border rounded pt-2 pb-4">
                <div class="row">
                    <div class="col-12">
                        <h2 class="card-subtitle text-center mt-2"><?=$company->company ?></h2>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="offset-1 col-10">
                        <h6 class="card-subtitle text-center "> Headquarter Address</h6>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="offset-1 col-4">
                        <h6 class="card-subtitle text-left ">Country:</h6>
                    </div>
                    <div class="col-6 pl-0">
                        <h6 class="card-subtitle text-right "><?= $company->country?></h6>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="offset-1 col-4">
                        <h6 class="card-subtitle text-left ">City:</h6>
                    </div>
                    <div class="col-6 pl-0">
                        <h6 class="card-subtitle text-right "><?= $company->city?></h6>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="offset-1 col-4">
                        <h6 class="card-subtitle text-left ">Street:</h6>
                    </div>
                    <div class="col-6 pl-0">
                        <h6 class="card-subtitle text-right "><?= $company->address?></h6>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="offset-1 col-5">
                        <h6 class="card-subtitle text-left ">Telephone:</h6>
                    </div>
                    <div class="col-5 pl-0">
                        <h6 class="card-subtitle text-right "><?= $company->telephone?></h6>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="offset-1 col-5">
                        <h6 class="card-subtitle text-left ">Email:</h6>
                    </div>
                    <div class="col-5 pl-0">
                        <h6 class="card-subtitle text-right "><?= $company->email?></h6>
                    </div>
                </div>
                <hr>
                <div class="col-12">
                    <h6 class="card-subtitle text-center mt-4">
                        <a class="btn btn-primary btn-block" role="button" href="/Enterprise/new_product"><i class="fas fa-plus"></i> Add
                            Product</a>
                    </h6>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-9">
            <?php include 'UI_elements/enterprise_product_list.php'?>
        </div>

    </div>
    <br>
</div>
