<!DOCTYPE html>
<html lang="en">
<?php include 'UI_elements/header.php'; ?>
<head>
    <title>Login | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Login Page for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, Account,Login,">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:23">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<main>
    <div class="container">
        <?php
        if (!empty($errors)) { ?>
            <div class="row">
                <div class="offset-2 col-8 alert alert-danger mt-3" role="alert">
                    <?= '<p class="text-center mb-0">' . $errors . '</p>'; ?>

                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="offset-3 col-6 panel panel-default">
                <div class="panel-heading text-center"><h1>Login</h1></div>
                <div class="panel-body">
                    <form method="post" action="/Account/login_function">
                        <div class="form-group">
                            <label for="loginEmail">User Email</label>
                            <input type="email" name="loginEmail" id="loginEmail" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="loginPassword">Password</label>
                            <input type="password" minlength="8" name="loginPassword" id="loginPassword"
                                   class="form-control">
                        </div>
                        <div class="form-check mb-2">
                            <input type="checkbox" name="loginRemember" id="loginRemember" class="form-check-input"
                                   value="0">
                            <label class="form-check-label" for="loginRemember">Remember me</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal"
                                            data-target=".bd-example-modal-recover" data-dismiss="modal">Recover
                                        Password
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-12 offset-md-4 col-md-4">
                                <div class="form-group text-right">
                                    <input type="submit" name="login" id="login" class="btn btn-info btn-block" value="Login">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
</body>
</html>