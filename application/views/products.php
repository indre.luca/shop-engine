<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/20/2019
 * Time: 3:33 PM
 */
include 'UI_elements/header_main_page.php';
echo HTML::style("../media/css/bootstrap.css");
echo HTML::style("../media/css/details.css");
echo HTML::script('../media/js/bootstrap-dropdownhover.js');
echo HTML::script('../media/js/jquery-3.4.1.min.js');
echo HTML::script('../media/js/script.js');
if (isset($_GET['page']) && $_GET['page'] > 0) {
    $page = $_GET['page'];
} else {
    $page = 1;
}
$itemsPerPageMin = 16 * ($page - 1);
$itemPerPageMax = $page * 16;
if ($number < $itemPerPageMax) {
    $itemPerPageMax = $number;
}
$count = 1;
?>
<head>
    <title>Shop Engine - Work in Progress for Progress</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Main page for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, User, Store, Products, Main Page, Index, Search, Categories">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:22">
</head>
<?php include 'UI_elements/error_handling.php'?>
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-3 col-sm-12">
            <?php include 'UI_elements/products_filter.php';?>
        </div>
        <div class="col-md-9 col-sm-12" id="product_list">
            <?php include 'UI_elements/products_item_list.php'?>
        </div>
    </div>
</div>
