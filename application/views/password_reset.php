<?php include 'UI_elements/header.php';
echo HTML::script('../media/js/jquery-3.4.1.min.js');
echo HTML::style("../media/css/bootstrap.css");
echo HTML::style("../media/css/details.css");
if (!isset($verify) || $verify != 1) {
    include 'UI_elements/recover_false.php';
} else {
    include 'UI_elements/recover_true.php';
} ?>
<head>
    <title>Account Recovery | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Acount Recovery Page for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, Owner,Management, Product, Add, Enterprise">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:23">
</head>
