<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Register Page for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, Account, Register,">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:23">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
<main>
    <div class="container">

        <div class="row">
            <div class="offset-md-2 col-md-8">
                <div class="text-center mt-5 mb-5"><a href="../"><img src="/media/image/logo_2.png"  class="d-inline-block align-top" alt=""></a></div>
                <div class="panel panel-success border shadow-sm p-4">
                    <div class="panel-heading text-center mt-5"><h3>Welcome.</h3></div>
                    <div class="panel-body mt-5">
                        <form method="post" action="/Account/register_function">
                            <?php
                            if (!empty($errors)) { ?>
                                <div class="row">
                                    <div class="offset-2 col-8 alert alert-danger" role="alert">
                                        <?= '<p class="text-center mb-0">' . $errors . '</p>'; ?>

                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="firstName">First Name*</label>
                                <input type="text" name="first_name" id="firstName" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="lastName">Last Name*</label>
                                <input type="text" name="last_name" id="lastName" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="registerEmail">User Email*</label>
                                <input type="email" name="email" id="registerEmail" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="registerPassword">Password</label>
                                <input type="password" minlength="8" name="password" id="registerPassword"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="repeatPassword">Repeat Password</label>
                                <input type="password" minlength="8" name="repeat_password" id="repeatPassword"
                                       class="form-control">
                            </div>
                            <div class="form-group text-md-right text-center offset-md-2 col-md-8 mt-md-5">
                                <input type="submit" name="register" id="register" class="btn btn-block btn-info"
                                       value="Register">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>

</body>
</html>