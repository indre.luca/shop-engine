<?php
$product = $products->as_array();
include 'UI_elements/header.php';
echo HTML::style("../media/css/bootstrap.css");
echo HTML::style("../media/css/details.css");
?>
<head>
    <title><?=$product['product_name'] ?> | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Product page for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, Owner,Management, Product, Edit, Enterprise">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:23">
</head>
<div class="container">
    <div class="row">
        <div class="page-header mt-5 ml-5"><h1 class="page-title"><?php echo $product['product_name'] ?></h1></div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-7 col-md-4 col-lg-4 text-center">
            <img class="card-img-top img-thumbnail" style="max-height: 350px;max-width: 350px;" src="<?php echo $product['image_path'] ?>"
                 alt="<?php echo $product['product_name'] ?>">
            <div class="row">
            </div>
        </div>
        <div class="col-sm-5 col-md-8 col-lg-8">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <?php echo $product['description'] ?>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <?php if ($product['price'] != $product['discounted_price']) {
                        $percent = round(100 - $product['discounted_price'] * 100 / $product['price']); ?>
                        <div class="row">
                        <div class="col-lg-5  col-sm-6 col-md-6 align-self-end"></div>
                        <div class="col-lg-3 p-0 text-right"><h6 class="">(-<?php echo $percent ?>%)</h6></div>
                        <div class="col-lg-4 pl-0 text-right"><h6><strike><?php echo $product['price'] ?>$</strike></h6>
                        </div>
                        </div><?php } ?>
                    <div class="row">
                        <div class="offset-lg-2 col-lg-10 align-self-end"><h3
                                    class="text-right"><?php echo $product['discounted_price'] ?>$</h3></div>
                    </div>
                    <hr>
                    <form class="mb-0" method="post" action="/Cart/add_to_cart">
                        <div class="row">
                            <input type="hidden" name="item_id" value='<?php echo $product['id'] ?>'>
                            <div class="offset-lg-2 col-lg-10 align-self-center">
                                <button type="submit" class="btn btn-primary btn-block">Add to Cart <i class="fas fa-cart-plus"></i></button>
                            </div>
                        </div>
                        <br>
                    </form>
                    <form method="post" action="/Account/add_to_wishlist">
                        <input type="hidden" name="item_id" value='<?php echo $product['id']?>'>
                        <div class="row">
                            <div class="offset-lg-2 col-lg-10 align-self-center">
                                <button type="submit" class="btn btn-outline-primary btn-block">Add to Wishlist <i class="fas fa-heart"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>