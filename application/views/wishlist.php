<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/25/2019
 * Time: 5:09 PM
 */
include 'UI_elements/header.php';
echo HTML::style("../media/css/bootstrap.css"); ?>
<head>
    <title>Wishlish | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Wishlist Page for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, Account, Wishlist, User, Products">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:23">
</head>
<div class="container mt-5">
    <div class="row">
        <div class="col-sm-12 offset-md-1 col-md-10">
            <?php if ($count > 0) {
                foreach ($wishlist as $id) {
                    $product = ORM::factory('Product', $id->product_id)->as_array();
                    //    ORM::factory('Products',$product[])?>
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12 align-self-center">
                            <img class="card-img-top" style="height: 120px; width: 120px;"
                                 src="<?php echo $product['image_path'] ?>" alt="">
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 align-self-center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h4 class="text-center"><?php echo $product['product_name'] ?></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <p><?php echo $product['description'] ?></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 align-self-center">
                            <?php if ($product['price'] != $product['discounted_price']) {
                                $percent = round(100 - $product['discounted_price'] * 100 / $product['price']); ?>
                                <div class="row">
                                <div class="offset-lg-4 col-lg-4"><h6
                                            class="text-right strikethrough"><?php echo $product['price'] ?>$</h6>
                                </div>
                                <div class="col-lg-4"><h6 class=""> (-<?php echo $percent ?>%)</h6></div>
                                </div><?php } ?>
                            <div class="row">
                                <div class="offset-lg-2 col-lg-10 align-self-end"><h6
                                            class="text-right"><?php echo $product['discounted_price'] ?>$</h6></div>
                            </div>
                            <hr>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 align-self-center">
                            <form method="post" action="/Cart/add_to_cart">
                                <div class="row">
                                    <input type="hidden" name="item_id" value='<?php echo $product['id'] ?>'>
                                    <div class="offset-lg-2 col-lg-10 align-self-center">
                                        <button type="submit" class="btn btn-outline-primary btn-block">Add to Cart <i class="fas fa-cart-plus"></i></button>
                                    </div>
                                </div>
                            </form>
                            <form method="post" action="/Account/remove_from_wishlist">
                                <div class="row">
                                    <input type="hidden" name="item_id" value='<?php echo $product['id'] ?>'>
                                    <div class="offset-lg-2 col-lg-10 align-self-center">
                                        <button type="submit" class="btn btn-outline-danger btn-block"><i class="far fa-trash-alt"></i> Remove</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php }
            } else { ?>
                <div class="row">
                    <div class="col-8">
                        <div class="alert alert-primary mt-5 text-center">
                            There are no items in your wishlist.
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

    </div>
    <br>
</div>
