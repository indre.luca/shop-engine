<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/25/2019
 * Time: 5:09 PM
 */
include 'UI_elements/header.php';
echo HTML::style("../media/css/bootstrap.css");
echo HTML::style("../media/css/details.css"); ?>
<head>
    <title>Cart | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="User Cart for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Shop, Engine, User, Cart, Products, Billing, Information,">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:24">
</head>
<div class="container">
    <div class="row">
        <div class="col-sm-12 offset-md-1 col-md-10">
            <?php if ($count > 0) {
                foreach ($cart_items as $id) {
                    $product = ORM::factory('Product', $id->product_id)->as_array();
                    //    ORM::factory('Product',$product[])?>
                    <div class="row">
                        <div class="col-lg-2 col-md-3 col-sm-12 align-self-center">
                            <img class="card-img-top" style="height: 120px; width: 120px;"
                                 src="<?php echo $product['image_path'] ?>" alt="">
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 align-self-center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h4 class="text-center"><?php echo $product['product_name'] ?></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <p class="product_details mb-2"><?php echo $product['description'] ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 align-self-center">
                            <?php if ($product['price'] != $product['discounted_price']) {
                                $percent = round(100 - $product['discounted_price'] * 100 / $product['price']); ?>
                                <div class="row">
                                <div class="col-lg-8 align-self-end"><h6 class="text-left">(-<?php echo $percent ?>
                                        %)</h6></div>
                                <div class="col-lg-4 align-self-end"><h6
                                            class="text-right strikethrough"><?php echo round($product['price']) ?>
                                        $</h6>
                                </div>
                                </div><?php } ?>
                            <div class="row">
                                <div class="col-lg-4 align-self-end"><h6
                                            class="text-right">Price:</h6></div>
                                <div class="col-lg-8 align-self-end"><h6
                                            class="text-right"><?php echo $product['discounted_price'] * $id->amount ?>
                                        $</h6></div>
                            </div>
                            <hr>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 align-self-center">
                            <form method="post" action="/Cart/update_cart">
                                <div class="row">
                                    <div class="form-group offset-lg-2 col-lg-10 align-self-center">
                                        <label for="amount_<?php
                                        echo $product['id'] ?>">Amount</label>
                                        <input type="hidden">
                                        <select name="amount" id="amount_<?php
                                        echo $product['id'] ?>" class="form-control">
                                            <?php if ($id->amount == 1) {
                                                echo '<option value="' . $id->amount . '" selected>' . $id->amount . '</option>';
                                                $n = 50;
                                                if ($product['stock'] < $n) $n = $product['stock'];
                                                for ($i = 2; $i <= $n; $i++) {
                                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                                }
                                            } elseif ($id->amount > 1 && $id->amount < 50) {
                                                for ($i = 1; $i < $id->amount; $i++) {
                                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                                }
                                                echo '<option value="' . $id->amount . '" selected>' . $id->amount . '</option>';
                                                $n = 50;
                                                if ($product['stock'] < $n) $n = $product['stock'];
                                                for ($i = $id->amount + 1; $i <= $n; $i++) {
                                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                                }
                                            } else {
                                                $n = 50;
                                                if ($product['stock'] < $n) $n = $product['stock'];
                                                for ($i = 1; $i < $n; $i++) {
                                                    echo '<option value="' . $i . '">' . $i . '</option>';
                                                }
                                                echo '<option value="' . $id->amount . '" selected>' . $id->amount . '</option>';

                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <input type="hidden" name="item_id" value='<?php echo $product['id'] ?>'>
                                    <div class="offset-lg-2 col-lg-10 align-self-center">
                                        <button type="submit" class="btn btn-outline-info btn-block"><i class="fas fa-arrow-up"></i> Update Cart</button>
                                    </div>
                                </div>
                            </form>
                            <form method="post" action="/Account/add_to_wishlist">
                                <div class="row">
                                    <input type="hidden" name="item_id" value='<?php echo $product['id'] ?>'>
                                    <div class="offset-lg-2 col-lg-10 align-self-center">
                                        <button type="submit" class="btn btn-outline-primary btn-block p-2"><i class="fas fa-heart"></i> Add to Wishlist</button>
                                    </div>
                                </div>
                            </form>
                            <form method="post" action="/Cart/remove_from_cart">
                                <div class="row">
                                    <input type="hidden" name="item_id" value='<?php echo $product['id'] ?>'>
                                    <div class="offset-lg-2 col-lg-10 align-self-center">
                                        <button type="submit" class="btn btn-outline-danger btn-block"><i class="far fa-trash-alt"></i> Remove</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php } ?>
                <hr>
                <div class="row">
                    <div class="offset-6 col-3">
                        <form method="post" action="/Cart/empty_cart">
                            <div class="row">
                                <div class="offset-lg-2 col-lg-10 align-self-center">
                                    <button type="submit" class="btn btn-danger btn-block"><i class="far fa-trash-alt"></i> Empty Cart</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-3">
                        <form method="post" action="/Cart/checkout">
                            <div class="row">
                                <div class="offset-lg-2 col-lg-10 align-self-center">
                                    <button type="submit" class="btn btn-success btn-block"><i class="fas fa-wallet"></i> Checkout</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="col-8">
                        <div class="alert alert-primary mt-5 text-center">
                            Your cart is empty.
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

    </div>
    <br>
</div>
