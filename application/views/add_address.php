<!DOCTYPE html>
<html lang="en">
<?php include 'UI_elements/header.php'; ?>
<head>
    <title>Add Address | Shop Engine</title>
    <link rel='icon' href="../media/image/favicon.png" type='image/x-icon' sizes="16x16" />
    <meta charset="UTF-8">
    <meta name="description"
          content="Address adding for the Users of the Shop Engine">
    <meta name="copyright"
          content="The project made for the Internship of Ranking Coach by Indre Gianluca">
    <meta name="keywords" content="Engine, User, Address, Add, Billing, Information,">
    <meta name="author" content="Indre Gianluca Willer">
    <meta name="last modified" content="17/11/2019 14:21">
</head>
<body>
<main>

</main>
</body>
</html>