<?php


/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/19/2019
 * Time: 1:09 PM
 */
class Controller_Account extends Controller
{
    #Login Logic

    public function action_login($errors = NULL)
    {
        if (Auth::instance()->logged_in()) {
            header('Location:' . $_SERVER['SERVER_NAME']);
        } else {
            $this->response->body(View::factory('login')->bind('errors', $errors));
        }
    }

    public function action_get_address_modals()
    {
        $user = Auth::instance()->get_user();
        $address = ORM::factory('Address')->where('user_id', '=', $user->id)->and_where('is_active', '=', 1)->find_all();
        $count = count($address);
        $this->response->body(View::factory('UI_elements/account_address_modal')->bind('addres', $address)->bind('count', $count));
    }

    public function action_login_function($errors = NULL)
    {
        if (Auth::instance()->logged_in()) {
            $this->action_Profile();
        } else {
            $post = $this->request->post();
            $remember = isset($_POST['loginRemember']);
            $success = Auth::instance()->login($post['loginEmail'], $post['loginPassword'], $remember);
            if ($success) {
                $model = new Model_Account();
                $model->admin_verification();
                $model->owner_verification();
                header('Location:../');
                exit();
            } else {
                $errors = 'The email or password are incorrect. Please check for any errors and try again.';
                $this->action_login($errors);
            }
        }
    }

    #Register Logic

    public function action_register()
    {
        if (Auth::instance()->get_user() == NULL)
            $this->response->body(View::factory('register'));
        else
            $this->action_Profile();
    }

    public function action_register_function()
    {
        $model = new Model_Account();
        $post = $this->request->post();
        if ($post['password'] == $post['repeat_password']) {
            $valid = $model->register($post['email'], $post['first_name'], $post['last_name'], $post['password']);
            if ($valid == FALSE) {
                $error = 'The email entered has already been registered.';
                $this->response->body(View::factory('register')->bind('errors', $error));
            } else {
                $model->send_token_email($post['email'], 'activate');
                echo 'Account successfully created. Please verify your email to activate the account.';
                header("refresh:5;url=../");
            }
        } else {
            $error = 'The passwords do no match.';
            $this->response->body(View::factory('register')->bind('errors', $error));
        }
    }

    public function action_verify()
    {
        $model = new Model_Account();
        $string = explode('.', $_GET['token']);
        $id = $string[0];
        $token = $string[1];
        $verify = $model->email_verification($id, $token);
        if ($verify) {
            $this->action_login();
        } else {
            echo "error";
        }
    }

    #Change Password Login

    public function action_change_password($errors = NULL)
    {
        $success = Auth::instance()->get_user();
        if ($success) {
            $this->response->body(View::factory('newPassword')->bind('errors', $errors));
        } else {
            $this->action_login();
        }
    }

    public function action_change_password_function()
    {
        if (Auth::instance()->get_user() == NULL) {
            $this->action_login('You are not currently logged in');
        } else {
            $post = $this->request->post();
            if ($post['oldPassword'] != NULL && $post['newPassword'] != NULL && $post['repeatPassword'] != NULL) {
                $oldPassword = $password = hash_hmac('sha256', $post['oldPassword'], 'test');
                $newPassword = $password = hash_hmac('sha256', $post['newPassword'], 'test');
                $conditionOne = $oldPassword == Auth::instance()->get_user()->password;
                $conditionTwo = $post['newPassword'] == $post['repeatPassword'];
                if ($conditionOne && $conditionTwo) {
                    $user = ORM::factory('User', Auth::instance()->get_user()->id);
                    $user->password = $newPassword;
                    $user->save();
                    $this->action_logout();
                } else {
                    if ($conditionOne || (!$conditionTwo && !$conditionOne)) {
                        $this->action_change_password('Could not verify the old password.');

                    } elseif ($conditionTwo) {
                        $this->action_change_password('The new passwords do not match.');
                    }
                }
            } else {
                $this->action_change_password('There are empty fields.');
            }
        }
    }

    #Wishlist

    public function action_add_to_wishlist()
    {
        if (Auth::instance()->get_user() == NULL) {
            $this->action_login('Please log in to add to wishlist');
        } else {
            $model = new Model_Account();
            $post = $this->request->post();
            $status = $model->add_to_wishlist($post['item_id']);
            if ($status) {
                header('Location: ../');
            } else {
                header('Location: ../');
            }
        }
    }
    public function action_add_to_wishlist_modal()
    {
        if (Auth::instance()->get_user() == NULL) {
            $this->action_login('Please log in to add to wishlist');
        } else {
            $model = new Model_Account();
            $post = $this->request->post();
            $status = $model->add_to_wishlist($post['item_id']);
            if ($status) {
                $this->response->status(200);
            } else {
                $this->response->status(403);
            }
        }
    }

    public function action_remove_from_wishlist()
    {
        if (Auth::instance()->get_user() == NULL) {
            $this->action_login('Please log in to add to wishlist');
        } else {
            $model = new Model_Account();
            $post = $this->request->post();
            $status = $model->remove_from_wishlistt($post['item_id']);
            if ($status) {
                $this->action_wishlist('An item has been removed from wishlist');
            } else {
                echo 'fail';
            }
        }
    }

    public function action_wishlist($warning = NULL)
    {
        if (Auth::instance()->get_user() == NULL) {
            $this->action_login('Please log in to add to wishlist');
        } else {
            $user_id = Auth::instance()->get_user('id');
            $wishlist = ORM::factory('Wishlist')->where('user_id', '=', $user_id)->find_all();
            $count = ORM::factory('Wishlist')->where('user_id', '=', $user_id)->count_all();
            $this->response->body(View::factory('wishlist')->bind('wishlist', $wishlist)->bind('count', $count)->bind('warning', $warning));
        }
    }

    #Reset Password Function

    public function action_send_recovery_email()
    {
        $post = $this->request->post();
        $model = new Model_Account();
        $user = ORM::factory('user')->where('email','=',$post['email'])->find();
        if ($user->email != NULL) {
            $model->send_token_email($user->email, 'reset');
        }else{

        }
    }
    public function action_reset(){
        $post = $this->request->post();
        $password = $post['password'];
        $repeat_password = $post['repeat_password'];
        $account_id = $post['account_id'];
        if($password != $repeat_password){
            header('Location: /Store/coming_soon');
        }else{
            $model = new Model_Account();
            $model->reset_password_and_delete_cookie($account_id,$password);
            header('Location:../');
        }
    }
    public function action_Recover()
    {
        if (isset($_GET['token'])) {
            $model = new Model_Account();
            $string = explode('.', $_GET['token']);
            $id = $string[0];
            $token = $string[1];
            $verify = $model->recover_email_verification($id, $token);
            $this->response->body(View::factory('password_reset')
                ->bind('verify', $verify)
                ->bind('user_id', $id));
        } else {
            $this->response->body(View::factory('recover_password'));
        }
    }

    #Address

    public function action_edit_address()
    {
        $success = Auth::instance()->get_user();
        if ($success) {
            $post = $this->request->post();
            $address = ORM::factory('Address', $post['address_id']);
            $this->response->body(View::factory('edit_address')->bind('address', $address));
        } else {
            $this->action_login();
        }
    }

    public function action_save_information()
    {
        $post = $this->request->post();
        if (isset($post['user_id'])) {
            $user = ORM::factory('User', $post['user_id']);
            $user->first_name = $post['user_first_name'];
            $user->last_name = $post['user_last_name'];
            $user->email = $post['user_email'];
            $user->mobile_telephone = $post['mobile_telephone'];
            $user->landline_telephone = $post['landline_telephone'];
            $user->save();
            $this->response->body(View::factory('UI_elements/account_information')->bind('user', $user));
        }
    }

    public function action_save_address()
    {
        $post = $this->request->post();
        $user = Auth::instance()->get_user();
        if (isset($post['address_id'])) {
            $address = ORM::factory('Address', $post['address_id']);
            $address->name = $post['address_name'];
            $address->country = $post['address_country'];
            $address->city = $post['address_city'];
            $address->address = $post['address_address'];
            $address->save();

        } else {
            $address = ORM::factory('Address');
            $count = $address->where('user_id', '=', $user->id)->count_all();
            if ($count == 0) {
                $default = 1;
            } elseif ($post['first_address']==1) {
                $default = 1;
            }else{
                $default = 0;
            }
            $data = Array(
                'user_id' => $user->id,
                'name' => $post['address_name'],
                'country' => $post['address_country'],
                'city' => $post['address_city'],
                'address' => $post['address_address'],
                'is_default' => $default
            );
            $address->values($data)->create();
        }
        $address = ORM::factory('Address')->where('user_id', '=', $user->id)->and_where('is_active', '=', 1)->find_all();
        $count = count($address);
        $this->response->body(View::factory('UI_elements/account_address')->bind('addres', $address)->bind('count', $count));
    }

    public function action_add_address()
    {
        $success = Auth::instance()->get_user();
        if ($success) {
            $this->response->body(View::factory('add_address'));
        } else {
            $this->action_login();
        }
    }

    public function action_Orders()
    {
        $success = Auth::instance()->get_user();
        if ($success) {
            $this->response->body(View::factory('order_history'));
        } else {
            $this->action_login();
        }
    }

    public function action_edit_information()
    {
        $success = Auth::instance()->get_user();
        if ($success) {
            $this->response->body(View::factory('edit_information')->bind('user', $success));
        } else {
            $this->action_login();
        }
    }

    public function action_delete_address()
    {
        $post = $this->request->post();
        $user_id = Auth::instance()->get_user('id');
        $address = ORM::factory('Address')->where('user_id', '=', $user_id)->and_where('id', '=', $post['address_id'])->find();
        $address->is_active = 0;
        $address->save();
        $address = ORM::factory('Address')->where('user_id', '=', $user_id)->and_where('is_active', '=', 1)->find_all();
        $count = count($address);
        $this->response->body(View::factory('UI_elements/account_address')->bind('addres', $address)->bind('count', $count));
    }

    public function action_set_default_address()
    {
        $post = $this->request->post();
        $user_id = Auth::instance()->get_user('id');
        $address = ORM::factory('Address')->where('user_id', '=', $user_id)->find_all();
        foreach ($address as $defaultAddress) {
            if ($defaultAddress->id == $post['address_id']) {
                $defaultAddress->is_default = 1;
                $defaultAddress->save();
            } else {
                $defaultAddress->is_default = 0;
                $defaultAddress->save();
            }
        }
        $address = ORM::factory('Address')->where('user_id', '=', $user_id)->and_where('is_active', '=', 1)->find_all();
        $count = count($address);
        $this->response->body(View::factory('UI_elements/account_address')->bind('addres', $address)->bind('count', $count));
    }

    public function action_logout()
    {
        Auth::instance()->logout();
        $session = Session::instance();
        $session->destroy();
        header('Location:../');
        exit();
    }

    public function action_Profile()
    {
        $user = Auth::instance()->get_user();
        $address = ORM::factory('Address')->where('user_id', '=', $user->id)->and_where('is_active', '=', 1)->find_all();
        $count = count($address);
        $this->response->body(View::factory('account')->bind('user', $user)->bind('addres', $address)->bind('count', $count));

    }
}
