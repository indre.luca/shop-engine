<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/28/2019
 * Time: 1:23 PM
 */

class Controller_Enterprise extends Controller
{
    public function action_Products()
    {
        if (Auth::instance()->get_user()) {
            $session = Session::instance();

            if ($session->get('admin') == 1 || $session->get('owner')) {
                $id = Auth::instance()->get_user('id');
                $model = new Model_Enterprise();
                $products = $model->get_business_products($id);
                $company = $model->get_business($id);
                $count = count($products);
                $view = $this->action_get_products($products, $company, $count);
            } else {
                echo 'You do not have permission to view this page.<br><a href="../">Click here to return to the main page</a>';
            }
        } else {
            $error = 'You are not logged in';
            $this->response->body(View::factory('login')->bind('errors', $error));
        }
    }

    public function action_get_products($products, $company, $count = 0)
    {
        $this->response->body(View::factory('enterprise_products')
            ->bind('product_id', $products)
            ->bind('count', $count)
            ->bind('company', $company));
    }

    public function action_status_change()
    {

    }

    public function action_new_product()
    {
        $categories = ORM::factory('Category')->find_all();
        $this->response->body(View::factory('add_product')->bind('categories', $categories));
    }

    public function action_edit_product()
    {
        $post = $this->request->post();
        $model = new Model_Enterprise();
        $validate_claim = $model->edit_product_ownership_validation($post['item_id']);
        if ($validate_claim) {
            $product = ORM::factory('Product', $post['item_id']);
            $category_id = ORM::factory('Products_Category')->where('product_id', '=', $post{'item_id'})->find();
            if ($category_id != NULL) {
                $dbpull = ORM::factory('Category');
                $categories = $dbpull->find_all();
                $categories_count = $dbpull->count_all();
                $product_category = ORM::factory('Category', $category_id->category_id)->as_array();

            }
            $this->response->body(View::factory('edit_product')->bind('product', $product)->bind('product_category', $product_category)->bind('categories', $categories)->bind('count', $categories_count));
        } else {
            echo "fail";
        }
    }

    public function action_save()
    {
        $post = $this->request->post();
        $name = $post['product_name'];
        $price = $post['product_price'];
        $description = $post['product_description'];
        $stock = $post['product_stock'];
        $category = $post['product_category'];
        if ($post['product_price'] < $post['product_discount']) {
            $discount = $post['product_price'];
        } else {
            $discount = $post['product_discount'];
        }
        $model = new Model_Enterprise();
        $validate_claim = $model->edit_product_ownership_validation($post['product_id']);
        if ($validate_claim) {
            $product = ORM::factory('Product', $post['product_id']);
            $product_category = ORM::factory('Products_Category')->where('product_id', '=', $post['product_id'])->find();
            if ($product->loaded() && $product_category->loaded()) {
                $product->product_name = $name;
                $product->description = $description;
                //$product->image_path=$item_image;
                $product->price = $price;
                $product->discounted_price = $discount;
                $product->stock = $stock;
                $product->save();
                $product_category->category_id = $category;
                $product_category->save();
                header('Location:../Enterprise/Products');
                exit();
            } else {
                echo 'fail';
                #TODO}
            }
        } else {
            echo 'fail2';
            #TODO
        }
    }

    public function action_add_product()
    {
        $post = $this->request->post();
        $user_id = Auth::instance()->get_user('id');
        $company = ORM::factory('Company')->where('user_id', '=', $user_id)->find();
        $name = $post['product_name'];
        $price = $post['product_price'];
        $description = $post['product_description'];
        $stock = $post['product_stock'];
        $category = $post['product_category'];

        if ($post['product_price'] < $post['product_discount']) {
            $discount = $post['product_price'];
        } else {
            $discount = $post['product_discount'];
        }
        $model = new Model_Enterprise();
        $product = ORM::factory('Product')->values(array(
            'product_name' => $name,
            'price' => $price,
            'discounted_price' => $price,
            'image_path' => '../media/image/lorem-ipsum.jpg',
            'description' => $description,
            'stock' => $stock,
        ))->create();
        ORM::factory('Products_Category')->values(array(
            'product_id' => $product->id,
            'category_id' => $category,
        ))->create();
        ORM::factory('Products_Company')->values(array(
            'product_id' => $product->id,
            'company_id' => $company->id,
        ))->create();
        header('Location:../Enterprise/Products');
        exit();
    }
    public function action_join(){

    }

    public function action_delete()
    {
        $post = $this->request->post();
        $model = new Model_Enterprise();
        $validate_claim = $model->edit_product_ownership_validation($post['item_id']);
        if ($validate_claim) {
            $product = ORM::factory('Product', $post['item_id']);
            if ($product->loaded()) {
                ORM::factory('Retired_Product')->values(array(
                    'product_id' => $product->id,
                    'product_name'=> $product->product_name,
                    'description'=> $product->description,
                    'price' => $product->price,
                    'discounted_price' => $product->discounted_price
                ))->create();
                $product->delete();
                header('Location:../Enterprise/Products');
            } else {
                #TODO
            }
        } else {
            #TODO
        }
    }
}