<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/20/2019
 * Time: 3:35 PM
 */

class Controller_Products extends Controller
{
    public function action_product()
    {
        $model = ORM::factory('Product');
        $products = $model->find_all();
        $this->response->body(
            View::factory('products')->bind('model', $products)
        );
    }
}