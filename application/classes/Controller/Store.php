<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/20/2019
 * Time: 3:35 PM
 */

class Controller_Store extends Controller
{
    public function action_index()
    {
        $products = ORM::factory('Product')->find_all();
        $count = count($products);
        $categories = ORM::factory('Category')->find_all();
        $this->response->body(
            View::factory('products')
                ->bind('product', $products)
                ->bind('number', $count)
                ->bind('categories', $categories)
        );
    }

    public function action_search()
    {
        $model = new Model_Filter();
        $post = $this->request->post();
        if (isset($_GET['search'])) {
            $products = $model->search($_GET['search']);
        } elseif (isset($post['search'])) {
            $products = $model->search($post['search']);
        } elseif (isset($_GET['category'])) {
            $products = $model->by_category($_GET['category']);
        } elseif (isset($post['category'])) {
            $products = $model->by_category($post['category']);
        } elseif (isset($_GET['min']) || isset($_GET['max'])) {
            if (!isset($_GET['min'])) {
                $products = $model->by_price('0', $_GET['max']);
            } elseif (!isset($_GET['max'])) {
                $products = $model->by_price($_GET['min']);
            } else {
                $products = $model->by_price($_GET['min'], $_GET['max']);
            }
        } elseif (isset($post['min']) || isset($post['max'])) {
            if (!isset($post['min'])) {
                $products = $model->by_price('0', $post['max']);
            } elseif (!isset($post['max'])) {
                $products = $model->by_price($post['min']);
            } else {
                $products = $model->by_price($post['min'], $post['max']);
            }
        } else {
            $products = ORM::factory('Product')->find_all();
        }
        $count = count($products);
        $categories = ORM::factory('Category')->find_all();
        $this->response->body(
            View::factory('UI_elements/products_item_list')
                ->bind('product', $products)
                ->bind('number', $count)
                ->bind('categories', $categories)
        );
    }

    public function action_coming_soon()
    {
        $this->response->body(View::factory('work_in_progress'));
    }

    public function action_details()
    {
        if (isset($_GET['id'])) {
            $product_id = $_GET['id'];
        } else {
            $post = $this->request->post();
            $product_id = $post['item_id'];
        }
        $product = ORM::factory('Product', $product_id);
        $this->response->body(View::factory('product_details')->bind('products', $product));
    }
}