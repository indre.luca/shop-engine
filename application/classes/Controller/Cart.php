<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/25/2019
 * Time: 3:58 PM
 */

class Controller_Cart extends Controller
{
    public function action_add_to_cart()
    {
        $model = new Model_Customer();
        $post = $this->request->post();
        $success = Auth::instance()->get_user();
        if ($success) {
            if (isset($post['amount'])) {
                $result = $model->add_to_cart($post['item_id'], $post['amount']);
            } else {
                $result = $model->add_to_cart($post['item_id']);
            }
            if ($result) {
                header('Location:../');
                exit();
            } else {
                echo 'bye';
            }
        } else {
            header('Location: ../Account/login');
        }

    }

    public function action_View($error = NUll, $success = NULL)
    {
        $user_id = Auth::instance()->get_user('id');
        $cart_items = ORM::factory('Cart_Item')->where('user_id', '=', $user_id)->find_all();
        $cart_count = ORM::factory('Cart_Item')->where('user_id', '=', $user_id)->count_all();
        $this->response->body(View::factory('cart')->bind('cart_items', $cart_items)->bind('count', $cart_count)->bind('error', $error)->bind('success', $success));
    }

    public function action_remove_from_cart()
    {
        $post = $this->request->post();
        $model = new Model_Customer();
        $remove = $model->remove_form_cart($post['item_id']);
        if ($remove) {
            #TODO
            $this->action_View();
        } else {
            #TODO
            echo 'fail';
        }
    }

    public function action_empty_cart()
    {
        $user_id = Auth::instance()->get_user('id');
        if ($user_id != NULL) {
            DB::delete('cart_items')->where('user_id', '=', $user_id)->execute();
        }
        header('Location:../Cart/View');
    }

    public function action_checkout($error = NULL)
    {
        $user = Auth::instance()->get_user();
        $model = new Model_Customer();
        $items = ORM::factory('Cart_Item')->where('user_id', '=', $user->id);
        $cart = $items->find_all();
        $cart_count = $items->count_all();
        $cost = $model->calculate_cart_cost($cart);
        $couriers = ORM::factory('Courier')->find_all();
        $products = ORM::factory('Product')->find_all();
        $addresses = ORM::factory('Address')->where('user_id', '=', $user->id)->and_where('is_active', '=', 1)->find_all();
        $payment_method = ORM::factory('Payment_Method')->find_all();
        $this->response->body(View::factory('checkout')
            ->bind('cart_items', $cart)
            ->bind('count',$cart_count)
            ->bind('products', $products)
            ->bind('cost', $cost)
            ->bind('couriers', $couriers)
            ->bind('addresses', $addresses)
            ->bind('methods', $payment_method)
            ->bind('error',$error));
    }

    public function action_complete_transaction()
    {
        $user_id = Auth::instance()->get_user('id');
        $post = $this->request->post();
        $model = new Model_Customer();
        if (!isset($post['address_id'])) {
            $this->action_checkout('address');
        } else {
            $address_id = $post['address_id'];
            $courier_id = $post['courier_id'];
            $payment_id = $post['method_id'];
            $model->checkout($user_id, $address_id, $courier_id, $payment_id);
            header('Location:../');
        }
    }

    public function action_update_cart()
    {
        $model = new Model_Customer();
        $post = $this->request->post();
        $result = $model->update_cart($post['item_id'], $post['amount']);
        if ($result) {
            $this->action_View(NULL, 'Item Updated');
        } else {
            $this->action_View('Error');
        }
    }
}