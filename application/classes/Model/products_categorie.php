<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/14/2019
 * Time: 3:23 PM
 */

class Model_products_category extends ORM
{
    protected $_belongs_to = array(
        'products' => array(
            'model'=> 'product',
            'foreign_key' => 'product_id',
        ),
        'categories' => array(
            'model'=> 'category',
            'foreign_key' => 'category_id',
        ),
    );
}
