<?php
/**
 * Created by PhpStorm.
 * User: Internship 2
 * Date: 10/28/2019
 * Time: 2:07 PM
 */

class Model_Product extends ORM {

    protected $_has_many = array(
        'orders' => array(
            'model'       => 'Order',
            'through' => 'Orders_Products',
            'update'  => FALSE
        ),
        'categories' => array(
            'model'       => 'Category',
            'through' => 'products_categories',
            'update'  => FALSE
        ),
    );
    protected $_has_one = array(
        'wishlists' => array(
            'model'       => 'Wishlist',
            'foreign_key' => 'product_id'
        ),
    );

}
