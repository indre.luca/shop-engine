<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/14/2019
 * Time: 2:55 PM
 */

class Model_Courier extends ORM
{
    protected $_has_one = array(
        'orders' => array(
            'model'       => 'Order',
            'foreign_key' => 'order_id'
        ),
    );
}