<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/29/2019
 * Time: 1:32 PM
 */

class Model_Enterprise
{
    public function get_business_products($user_id)
    {
        $products = ORM::factory('Products_Company')
            ->join('companies')
            ->on('companies.id','=','products_company.company_id')
            ->where('user_id', '=', $user_id)
            ->find_all();
        return $products;
    }
    public function get_business($user_id){
        $company = ORM::factory('Company')
            ->where('user_id','=',$user_id)->find();
        return $company;
    }

    public function edit_product_ownership_validation($product_id)
    {
        $user_id = Auth::instance()->get_user('id');
        if ($user_id != NULL) {
            $company = ORM::factory('Company')->where('user_id', '=', $user_id)->find();
            $products = ORM::factory('Products_Company')->where('company_id', '=', $company->id)->where('product_id', '=', $product_id)->find()->as_array();
            if ($products['id'])
                return TRUE;
            else
                return FALSE;
        } else {
            return FALSE;
        }
    }
}