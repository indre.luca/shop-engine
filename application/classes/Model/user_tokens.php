<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/18/2019
 * Time: 1:04 PM
 */
class Model_user_tokens extends Model_Auth_User_Token
{
    protected $_belongs_to = array(
        'users' => array(
            'model'=> 'user',
            'foreign_key' => 'user_id',
        ),
    );
}