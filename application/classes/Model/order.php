<?php
/**
 * Created by PhpStorm.
 * User: Internship 9
 * Date: 10/23/2019
 * Time: 1:17 PM
 */

class Model_Order extends ORM
{
    protected $_has_many = array(
        'products' => array(
            'model'       => 'order_product',
            'through' => 'orders_products',
            'update'  => FALSE
        ),
    );
    protected $_belongs_to = array(
        'User' => array(
            'model'=> 'User',
            'foreign_key' => 'user_id',
        ),
        'addresses' => array(
            'model'=> 'status',
            'foreign_key' => 'status_id',
        ),
        'order_status' => array(
            'model'=> 'order_status',
            'foreign_key' => 'order_status_id',
        ),
        'couriers' => array(
            'model'=> 'courier',
            'foreign_key' => 'courier_id',
        ),
    );
}