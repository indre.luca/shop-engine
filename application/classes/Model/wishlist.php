<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/18/2019
 * Time: 1:14 PM
 */

class Model_Wishlist extends ORM
{
    protected $_belongs_to = array(
        'User' => array(
            'model'=> 'User',
            'foreign_key' => 'user_id',
        ),
        'products' => array(
            'model'=> 'Product',
            'foreign_key' => 'product_id',
        ),
    );
}