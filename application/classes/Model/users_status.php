<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/18/2019
 * Time: 12:59 PM
 */

class Model_user_status extends ORM
{
    protected $_belongs_to = array(
        'users' => array(
            'model'=> 'user',
            'foreign_key' => 'user_id',
        ),
        'statuses' => array(
            'model'=> 'status',
            'foreign_key' => 'status_id',
        ),
    );
}