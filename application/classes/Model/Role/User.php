<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/19/2019
 * Time: 2:42 PM
 */

class Model_Roles_User extends ORM
{
    protected $_belongs_to = array(
        'User' => array(
            'model'       => 'User',
            'foreign_key' => 'user_id',
        ),
        'Role' => array(
            'model'       => 'Role',
            'foreign_key' => 'role_id',
        ),
    );
}