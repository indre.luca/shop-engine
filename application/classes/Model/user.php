<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/13/2019
 * Time: 3:42 PM
 */

class Model_User extends Model_Auth_User {
    protected $_has_many = array(
        'user_orders' => array(
            'model'       => 'Order',
            'foreign_key' => 'user_id'
        ),
        'addresses' => array(
            'model'       => 'Address',
        ),
        'user_wishlists' => array(
            'model'       => 'Wishlist',
            'foreign_key' => 'user_id'
        ),
        'roles' => array(
            'model'       => 'Role',
            'through' => 'roles_users',
            'update'  => FALSE
        ),
        'tokens' => array(
            'model'       => 'token',
            'foreign_key' => 'user_id'
        ),
    );
    protected $_has_one = array(

        'user_tokens' => array(
            'model'       => 'User/Tokens',
            'foreign_key' => 'user_id'
        ),

        'user_statuses' => array(
            'model'       => 'User/Status',
            'foreign_key' => 'user_id'
        ),

    );
    public function rules()
    {
        return array(
            'password' => array(
                array('not_empty'),
            ),
            'email' => array(
                array('not_empty'),
                array('email'),
                array(array($this, 'unique'), array('email', ':value')),
            ),
        );
    }

}
