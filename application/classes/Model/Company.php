<?php

/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/29/2019
 * Time: 1:51 PM
 */
class Model_Company extends ORM
{
    protected $_belongs_to = array(
        'User' => array(
            'model'=> 'User',
        ),
    );
    protected $_has_one = array(
        'products' => array(
            'model'       => 'products_companies',
            'foreign_key' => 'company_id',
        ),
    );
}