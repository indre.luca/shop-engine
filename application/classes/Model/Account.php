<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/22/2019
 * Time: 3:06 PM
 */

class Model_Account
{
    public function register($email, $firstname, $lastname, $password)
    {
        $emailVerification = ORM::factory('User')->where('email', '=', $email)->find();
        if ($emailVerification->email) {
            return FALSE;
        } else {
            $user = ORM::factory('User')->values([
                'first_name' => $firstname,
                'last_name' => $lastname,
                'email' => $email,
                'password' => $password,
            ]);
            $user->save();
            return TRUE;
        }
    }

    public function reset_password_and_delete_cookie($id, $password)
    {
        $user = ORM::factory('User', $id);
        $user->password = $password;
        $user->save();
        $token = ORM::factory('Token')->where('user_id', '=', $id)->and_where('token_type', '=', 'reset')->find();
        if ($token->loaded())
            $token->delete();
    }

    public function add_to_wishlist_verification($id)
    {
        $user_id = Auth::instance()->get_user('id');
        $item = ORM::factory('Wishlist')->where('user_id', '=', $user_id)->where('product_id', '=', $id)->count_all();
        if ($item == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function admin_verification()
    {
        $admin = ORM::factory('User')->join('roles_users')->on('user.id', '=', 'roles_users.user_id')
            ->where('roles_users.role_id', '=', 2)->where('id', '=', Auth::instance()->get_user('id'))->count_all();
        $session = Session::instance();
        if ($admin != 0) {
            $session->set('admin', 1);
        }
    }

    public function owner_verification()
    {
        $admin = ORM::factory('User')->join('roles_users')->on('user.id', '=', 'roles_users.user_id')
            ->where('roles_users.role_id', '=', 3)->where('id', '=', Auth::instance()->get_user('id'))->count_all();
        $session = Session::instance();
        if ($admin != 0) {
            $session->set('owner', 1);
        }
    }

    public function add_to_wishlist($id)
    {
        if ($this->add_to_wishlist_verification($id) == FALSE) {
            return FALSE;
        } else {
            $user_id = Auth::instance()->get_user('id');
            $wishlist = ORM::factory('Wishlist')->values([
                'user_id' => $user_id,
                'product_id' => $id,
            ]);
            $wishlist->save();
            return TRUE;
        }
    }

    public function remove_from_wishlistt($id)
    {
        if ($this->add_to_wishlist_verification($id) == TRUE) {
            return FALSE;
        } else {
            $user_id = Auth::instance()->get_user('id');
            $wishlist = ORM::factory('Wishlist')->where('user_id', '=', $user_id)->where('product_id', '=', $id)->find();
            $wishlist->delete();
            return TRUE;
        }
    }

    public function generate_token($length = 13)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function send_token_email($mail, $type)
    {
        $token_string = $this->generate_token();
        $id = ORM::factory('User')->where('email', '=', $mail)->find();
        $token = ORM::factory('Token');
        $checkOldToken = $token->where('user_id','=',$id)->and_where('token_type','=',$type)->find() ;
        if($checkOldToken->loaded())
            $checkOldToken->delete();
            $token->values([
            'user_id' => $id,
            'token' => $token_string,
            'token_type' => $type,
            'expire_date' => strtotime('+24 hours')
        ]);
        $token->save();
        switch ($type) {
            case 'activate':
                mail($mail, 'Account Verification Email', 'https://g_indre.internship.rankingcoach.com/Account/verify?token=' . $id . '.' . $token_string);
                break;
            case 'reset';
                mail($mail, 'Password retrieval was requested', 'https://g_indre.internship.rankingcoach.com/Account/recover?token=' . $id . '.' . $token_string);
                break;
        }
    }

    public function recover_email_verification($id, $token)
    {
        $dbEntry = ORM::factory('Token')->where('user_id', '=', $id)->where('token', '=', $token)->where('token_type', '=', 'reset')->find()->as_array();
        if ($dbEntry['expire_date'] > strtotime('now')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function email_verification($id, $token)
    {
        $dbEntry = ORM::factory('Token')->where('user_id', '=', $id)->where('token', '=', $token)->where('token_type', '=', 'activate')->find()->as_array();
        if ($dbEntry['expire_date'] > strtotime('now')) {
            $role = ORM::factory('Roles_User')->values([
                'user_id' => $id,
                'role_id' => 1,
            ]);
            $role->save();
            $loadToken = ORM::factory('Token')->where('user_id', '=', $id)->where('token', '=', $token)->find();
            if ($loadToken->loaded()) {
                $loadToken->delete();
                return TRUE;
            } else {
                return FALSE;
            }

        } else {
            $user = ORM::factory('User', $id);
            $loadToken = ORM::factory('Token')->where('user_id', '=', $id)->where('token', '=', $token)->find();
            if ($loadToken) {
                $loadToken->delete();
                $this->send_token_email($user->email, 'activate');
            }
            return FALSE;
        }
    }
}