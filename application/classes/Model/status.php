<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/18/2019
 * Time: 12:57 PM
 */

class Model_status extends ORM
{
    protected $_has_one = array(
        'users_statuses' => array(
            'model' => 'Users_Status',
            'foreign_key' => 'status_id'
        )
    );
}