<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/19/2019
 * Time: 2:45 PM
 */

class Model_role extends Model_Auth_Role
{
    protected $_has_many = array(
        'users' => array(
            'model'       => 'User',
            'through' => 'roles_users',
            'foreign_key'=>'user_id',
            'update'  => FALSE
        ),
    );
}