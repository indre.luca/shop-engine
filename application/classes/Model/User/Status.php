<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/18/2019
 * Time: 12:59 PM
 */

class Model_user_status extends ORM
{
    protected $_belongs_to = array(
        'User' => array(
            'model'=> 'User',
            'foreign_key' => 'user_id',
        ),
        'statuses' => array(
            'model'=> 'Status',
            'foreign_key' => 'status_id',
        ),
    );
}