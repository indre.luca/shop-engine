<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 12/4/2019
 * Time: 3:46 PM
 */

class Model_Payment_Method extends ORM
{
    protected $_has_many = array(
        'orders' => array(
            'model'       => 'Order',
            'foreign_key' => 'order_id',
        ),
    );
}