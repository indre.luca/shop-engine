<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/27/2019
 * Time: 3:12 PM
 */

class Model_Cart_Item extends ORM
{
    protected $_belongs_to = array(
        'Users' => array(
            'model'=> 'User',
            'foreign_key' => 'user_id',
        ),
        'Products' => array(
            'model'=> 'Product',
            'foreign_key' => 'product_id',
        ),
    );
}