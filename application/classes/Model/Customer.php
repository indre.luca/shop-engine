<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/25/2019
 * Time: 4:00 PM
 */

class Model_Customer
{
    public function verify_product_stock($id)
    {
        $stock = ORM::factory('Product', $id)->as_array();
        if ($stock['stock'] == 0) {
            return FALSE;
        }
        return TRUE;
    }

    public function add_to_cart($id, $amount = 1)
    {
        if ($this->verify_product_stock($id) == FALSE) {
            return FALSE;
        }
        $user_id = Auth::instance()->get_user('id');
        $item = ORM::factory('Cart_Item')->where('user_id', '=', $user_id)->where('product_id', '=', $id)->find();
        if ($item->amount == NULL) {
            $data = array(
                'user_id' => $user_id,
                'product_id' => $id,
                'amount' => $amount,
            );
            ORM::factory('Cart_Item')->values($data)->create();
        } else {
            if ($item->loaded()) {
                $item->amount = $item->amount + $amount;
                $item->save();
            }
        }
        return TRUE;
    }

    public function remove_form_cart($product_id)
    {
        $user_id = Auth::instance()->get_user('id');
        $item = ORM::factory('Cart_Item')->where('user_id', '=', $user_id)->where('product_id', '=', $product_id)->find();
        if ($item->loaded()) {
            $item->delete();
            return TRUE;
        }
        return FALSE;

    }
    public function calculate_cart_cost($items){
        $cost = 0;
        $products = ORM::factory('Product')->find_all();
        foreach($items as $item){
            foreach($products as $product)
            if($item->product_id == $product->id){
                $cost = $cost+($item->amount * $product->discounted_price);
            }
        }
        return $cost;
    }
    public function checkout($user_id,$address_id,$courier_id,$payment_id){
        #TODO Validations
        $cart_products = ORM::factory('Cart_Item')->where('user_id', '=', $user_id)->find_all();
        $data = array(
            'user_id' =>$user_id,
            'address_id' => $address_id,
            'status_id'=> 1,
            'payment_method_id'=>$payment_id,
            'courier_id'=>$courier_id,
            'date_order'=> strtotime('now'));
        $order = ORM::factory('Order')->values($data)->create();
        foreach($cart_products as $order_item){
            $price = ORM::factory('Product',$order_item->product_id);
            ORM::factory('Orders_Product')->values(array(
                'order_id'=>$order->id,
                'product_id'=>$order_item->id,
                'amount'=>$order_item->amount,
                'price_at_order'=>$price->discounted_price*$order_item->amount,
            ))->create();
            $price->stock=$price->stock-$order_item->amount;
            $price->save();
        }
        #TODO send mail
        DB::delete('cart_items')->where('user_id','=',$user_id)->execute();
    }

    public function update_cart($id, $amount = 1)
    {
        if ($this->verify_product_stock($id) == FALSE) {
            return FALSE;
        }
        $user_id = Auth::instance()->get_user('id');
        $item = ORM::factory('Cart_Item')->where('user_id', '=', $user_id)->where('product_id', '=', $id)->find();
        if ($item->loaded()) {
            $item->amount = $amount;
            $item->save();
        }
        return TRUE;
    }
}