<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/14/2019
 * Time: 3:23 PM
 */

class Model_Category extends ORM
{
    protected $_has_many = array(
        'orders' => array(
            'model' => 'Products',
            'through' => 'Products_Category',
            'update' => FALSE
        ),);
}
