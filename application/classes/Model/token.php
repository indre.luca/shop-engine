<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 25.11.2019
 * Time: 10:30
 */

class Model_Token extends ORM
{
    protected $_belongs_to = array(
        'user' => array(
            'model' => 'user',
            'foreign_key' => 'user_id',
        ),
    );
}