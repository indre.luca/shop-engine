<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/18/2019
 * Time: 1:13 PM
 */

class Model_order_status extends ORM
{
    protected $_has_one = array(
        'orders' => array(
            'model'       => 'Order',
            'foreign_key' => 'order_id'
        ),
    );
}