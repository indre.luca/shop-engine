<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/15/2019
 * Time: 2:22 PM
 */

class Model_Orders_Product extends ORM
{
    protected $_belongs_to = array(
        'products' => array(
            'model'       => 'Product',
            'foreign_key' => 'product_id',
        ),
        'orders' => array(
            'model'       => 'Order',
            'foreign_key' => 'order_id',
        ),
    );

}