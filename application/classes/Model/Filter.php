<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 12/6/2019
 * Time: 1:13 PM
 */

class Model_Filter
{
    public function by_category($category_id)
    {
        if ($category_id == 1) {
            $products = ORM::factory('Product')
                ->join('products_categories')
                ->on('product.id', '=', 'products_categories.product_id')
                ->join('categories')
                ->on('products_categories.category_id', '=', 'categories.id')
                ->where('categories.parent_id', '=', '2')
                ->or_where('categories.parent_id', '=', '3')
                ->find_all();
            return $products;
        } elseif ($category_id == 2 || $category_id == 3) {
            $products = ORM::factory('Product')
                ->join('products_categories')
                ->on('product.id', '=', 'products_categories.product_id')
                ->join('categories')
                ->on('products_categories.category_id', '=', 'categories.id')
                ->where('categories.parent_id', '=', $category_id)
                ->find_all();
            return $products;
        } else {
            $products = ORM::factory('Product')
                ->join('products_categories')
                ->on('product.id', '=', 'products_categories.product_id')
                ->where('products_categories.category_id', '=', $category_id)
                ->find_all();
            return $products;
        }
    }

    public function by_category_count($category_id)
    {
        $prod = ORM::factory('Product');
        $prod = $prod
            ->join('products_categories')
            ->on('product.id', '=', 'products_categories.product_id');
        if ($category_id == 1) {
            $category = 1;
        } elseif ($category_id == 2 || $category_id == 3) {
            $category = 2;
        } elseif ($category_id > 3 && $category_id <= 12) {
            $category = 3;
        } else
            $category = 0;
        switch ($category) {
            case '1':
                $prod = $prod->join('categories')
                    ->on('products_categories.category_id', '=', 'categories.id')
                    ->where('categories.parent_id', '=', '2')
                    ->or_where('categories.parent_id', '=', '3');
                break;
            case '2':
                $prod = $prod->join('categories')
                    ->on('products_categories.category_id', '=', 'categories.id')
                    ->where('categories.parent_id', '=', $category_id);
                break;
            case '3':
                $prod = $prod->where('products_categories.category_id', '=', $category_id);
                break;
            default:
                return 'error';
        }
        $products = $prod->find_all();

        return $products;
    }

    public function by_price($min = 0, $max = 4294967295)
    {
        $products = ORM::factory('Product')
            ->where('discounted_price', '>', $min)
            ->and_where('discounted_price', '<', $max)
            ->find_all();
        return $products;
    }

    public function search($input)
    {
        $products = ORM::factory('Product')
            ->where('product_name', 'LIKE', '%' . $input . '%')
            ->find_all();
        return $products;
    }

}