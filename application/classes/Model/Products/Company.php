<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/29/2019
 * Time: 1:54 PM
 */

class Model_Products_Company extends ORM
{
    protected $_belongs_to = array(
        'products' => array(
            'model'=> 'Product',
            'foreign_key' => 'product_id',
        ),
        'company' => array(
            'model'=> 'Company',
            'foreign_key' => 'category_id',
        ),
    );
}