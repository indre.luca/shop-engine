<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/14/2019
 * Time: 3:23 PM
 */

class Model_Products_Category extends ORM
{
    protected $_belongs_to = array(
        'product' => array(
            'model'=> 'Product',
            'foreign_key' => 'product_id',
        ),
        'category' => array(
            'model'=> 'Category',
            'foreign_key' => 'category_id',
        ),
    );
}
