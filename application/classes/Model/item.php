<?php
/**
 * Created by PhpStorm.
 * User: Internship 2
 * Date: 10/29/2019
 * Time: 2:42 PM
 */

class Model_Item extends ORM
{
    public function rules()
    {
        return array(
            'username' => array(
                array('not_empty'),
                array('min_length', array(':value', 4)),
                array('max_length', array(':value', 32)),
                array('regex', array(':value', '/^[-\pL\pN_.]++$/uD')),
            ),
            'first_name' => array(
            ),
            'last_name' => array(
            ),
            'email' => array(
            ),
        );
    }
}