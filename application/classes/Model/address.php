<?php
/**
 * Created by PhpStorm.
 * User: Indre Gianluca
 * Date: 11/14/2019
 * Time: 2:13 PM
 */
class Model_Address extends ORM{
    protected $_belongs_to = array(
        'User' => array(
            'model'=> 'User',
        ),
    );
    protected $_has_one = array(
        'orders' => array(
            'model'       => 'Order',
            'foreign_key' => 'order_id',
        ),
    );
}