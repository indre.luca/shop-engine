<?php

return array(
    "products" => array(
        "BRU001" => array(
            "title" => "Brushes",
            "item_image" => "/media/img/product.jpg",
            "description" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
            "unit" => "kg",
            "limits" => array(
                "min" => 1,
                "max" => 50
            ),
            "price" => 10,
            "currency" => "LEU"
        ),
        "ACR001" => array(
            "title" => "Acrylic",
            "item_image" => "/media/img/product3.jpg",
            "description" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
            "unit" => "kg",
            "limits" => array(
                "min" => 1,
                "max" => 50
            ),
            "price" => 33,
            "currency" => "LEU"
        ),
        "CTH001" => array(
            "title" => "Painting Cloth",
            "item_image" => "/media/img/product2.jpg",
            "description" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
            "unit" => "kg",
            "limits" => array(
                "min" => 1,
                "max" => 50
            ),
            "price" => 60,
            "currency" => "LEU"
        ),"FTR003" => array(
            "title" => "Developer Pad",
            "item_image" => "/media/img/product1.jpg",
            "description" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
            "unit" => "kg",
            "limits" => array(
                "min" => 1,
                "max" => 50
            ),
            "price" => 60,
            "currency" => "LEU"
        )
    ),
    "shipping" => array(
        "express" => array(
            "price" => 60
        ),
        "normal" => array(
            "price" => 25
        )
    ),
    "delivery" => array(
        "free_trigger" => 100
    )
);
?>