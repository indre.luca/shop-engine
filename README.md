##    **End User**
    
- browse the website using the search function or by browsing categories
- fine tune the result using filters
- add and remove products from cart
- select the type of courier
- register and log-in


###   **Optional (after basic functionality)**
    
- Send email to user with the details of his shipment
- Add a page with helpful information and methods of contacting a Moderator
- Allow the user to see his past orders if he has purchased anything while logged in
- Allow the user to choose his color scheme for the website out of a few generic options

##  **Manager/Admin**
    
 - Add new products for the Admin Page and/or modifity the current ones
 - Add new categories for products
 - Drag and drop functionality for Images

##    **Other Functionalities**
    
 - TODO